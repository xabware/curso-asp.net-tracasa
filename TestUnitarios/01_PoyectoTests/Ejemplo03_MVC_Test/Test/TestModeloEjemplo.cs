﻿
using Ejemplo03_MVC.DLL.Modelo;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo02_TestMVC.Test
{
    class TestModeloEjemplo
    {
        [Test(Author = "Xabier CV", Description = "Prueba el modelo de listas")]
        public void TestModeloListaEjemplo()
        {
            IModeloEjemplo model1;
            model1 = new ModeloEjemLista();

            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");
            ArrayList lista = new ArrayList((ICollection)model1.LeerTodos());
            Assert.AreEqual(lista.Count, 3);
        }
    }
}
