﻿using Ejemplo03_MVC.DLL.Modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC.DLL
{
    public class ControladorEjemplo
    {
        IModeloEjemplo modelo;

        public ControladorEjemplo(IModeloEjemplo modelo)
        {
            this.modelo = modelo;
            
        }

        public void AltaEjemplo(int entero, string str)
        {
            modelo.Crear(entero, str);
        }

        public IEnumerable<Ejemplo> LeerEjemplos()
        {
            return modelo.LeerTodos();
        }

        public Ejemplo LeerUno(string s)
        {
            return modelo.LeerUno(s);
        }

        public bool EliminarUno(string nombre)
        {
            return modelo.EliminarUno(nombre);
        }

        public bool ModificarNombreUno(Ejemplo ejemplo, string str)
        {
            return modelo.ModificarNombreUno(ejemplo,str);
        }

        public Ejemplo Modificar(string nombreBusq, int entero, string str)
        {
            return modelo.ModificarUno(nombreBusq, entero, str);
        }
    }
}
