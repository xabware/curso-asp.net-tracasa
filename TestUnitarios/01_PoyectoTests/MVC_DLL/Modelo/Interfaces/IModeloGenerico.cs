﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC.DLL.Modelo
{
    public interface IModeloGenerico<Tipo>
    {
        Tipo Crear(Tipo nuevoObj);
        IList<Tipo> LeerTodos();
        Tipo LeerUno<TBusq>(TBusq campo);
        bool Eliminar(int entero);
        Tipo Modificar(Tipo nuevoObj);

    }
}
