using NUnit.Framework;
using System;

namespace _01_PoyectoTests
{
    public class Tests
    {
        string texto;
        int numero;
        float real;
        double doble;

        //Este es el m�todo de inizializaci�n porque lleva el atributo setup
        [SetUp]
        public void Inicializacion()
        {
            texto = "Texto Inicial";
            numero = 1;
            real = 0.1f;
            doble = 0.2;
        }

        [Test]
        public void TestPrimero()
        {
            Assert.Pass();
            texto = "Texto Inicial1";
        }

        [Test]
        public void TestSegundo()
        {
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 3, 2 + 0);
        }

        [Test]
        public void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado Cualquiera OK");
        }

        [Test]
        public void DelegadoCualquieraMal()
        {
            throw new Exception("Delegado cualquiera mal");
        }



        [Test]
        public void Test3()
        {
            Console.WriteLine("Antes del Test3");
            Assert.IsTrue(texto.Equals("Texto Inicial"), "El texto no se ha inicializado");
            Assert.Contains('i', texto.ToCharArray(), "El array de char texto no tiene i");
            TestDelegate delegateOk = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk, "Delegado Cualquiera Ok fall�");

            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado cualquiera Mal fall�");
            Console.WriteLine("Despu�s del Test3");
        }

        [Test(Author ="Xabier CV",Description = "Comprueba si cualquier variable de tipo num�rico es positiva(int,float,double,uint,long,ulong y decimal)")]
        public void TestNumeroEsPositivoBien()
        {
            Assert.Positive(1);//int
            Assert.Positive(0.1f);//float
            Assert.Positive(0.2);//double
            Assert.Positive(2.1m);//decimal
        }

        [Test(Author = "Xabier CV", Description = "Comprueba si cualquier variable de tipo num�rico es positiva(int,float,double,uint,long,ulong y decimal)")]
        public void TestNumeroEsPositivoMal()
        {
            Assert.Positive(-1);
            Assert.Positive(0);
        }
    }
}