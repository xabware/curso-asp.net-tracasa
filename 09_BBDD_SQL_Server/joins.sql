﻿
/*
	 select SUM(c.Cantidad) from Producto as P, CompraUsuarioProducto as C, Usuario as U 
			where C.IdUsuario = U.Id and C.IdProducto = P.Id and U.edad <= 20 
		1 - Esta consulta con JOIN
		
	Por cada usuario, nombre de usuario y nombre de producto,
		2 - El precio del producto de sus compras
		3 - Cuanto se ha gastado en total en cada producto
		4 - Cuantas copias del producto ha comprado
	Por otro lado:
		5 - Todas las ventas (dinero) del 2020
		6 - Todos los usuarios que compraron en el 2019
		7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
		8 - El coche cuyo propietario ha comprado más a lo largo de la historia
		9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
		10 - Qué productos han comprado los usuarios del Mercedes
*/

/*
-- 1
select SUM(c.Cantidad) from Producto as P inner join CompraUsuProd as C on C.IdProducto = P.Id inner join Usuario as U on C.IdUsuario = U.Id
where U.edad <= 20 

-- 2
select U.nombre, P.nombre, sum(C.cantidad)*P.precio
from Usuario U left join CompraUsuProd C on U.Id=C.idUsuario right join Producto P on P.Id=C.idProducto 
group by U.nombre, C.idProducto,P.nombre, P.precio

--3

select P.nombre, sum(C.cantidad)*P.precio as total_gastado
from CompraUsuProd as C right join Producto as P on P.Id=C.idProducto 
group by C.idProducto,P.nombre, P.precio

--4
select P.nombre, sum(C.cantidad)
from CompraUsuProd as C right join Producto as P on P.Id=C.idProducto 
group by C.idProducto,P.nombre

--5

select sum(P.precio*C.cantidad) as total_ganado_2020
from CompraUsuProd C inner join Producto P on C.idProducto=P.Id
where C.fecha>='2020-01-01' and C.fecha <='2020-12-31'


--6

select U.nombre
from CompraUsuProd C inner join Usuario U on C.idProducto=U.Id
where C.fecha>='2019-01-01' and C.fecha <='2019-12-31'
group by U.Id, U.nombre


--7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020

select año, max(suma),nombreProd
from (select year(C.fecha) as año,P.id,P.nombre as nombreProd,sum(cantidad) as suma--sum(cantidad) as cuenta2019,P.nombre as nombre2019
from Producto P inner join CompraUsuProd C on P.Id=C.idProducto
group by year(C.fecha),P.Id,P.nombre) as subconsulta
group by año, nombreProd
*/

--8 El coche cuyo propietario ha comprado más a lo largo de la historia
select top(1) (C.Marca+ '-'+ C.Modelo) as coche, U.nombre, sum(cantidad) as suma
from Coche C 
	right join Usuario U on C.Id=U.id_coche 
	inner join CompraUsuProd CP on U.Id=CP.idUsuario
group by U.nombre,C.Marca,C.Modelo
order by suma desc