﻿
--select * from (select top 5 * from [dbo].[Usuario] where coche is null order by edad desc) as subconsulta order by nombre;

--devolver cuantos usuarios hay inactivos y cuantos activos

-- devolver activos e inactivos

/*
select count(Id)
from [dbo].[Usuario]
group by activo;
*/

/*
select count(distinct subinactivos.inactivos), count(distinct subactivos.activos)
from (select Id as activos from [dbo].[Usuario] where activo = 1) as subactivos,(select Id as inactivos from [dbo].[Usuario] where activo = 0) as subinactivos
*/

/*
select count(case activo when 1 then 1 end),count(case activo when 0 then 1 end)
from [dbo].[Usuario]
*/

/*
SELECT COUNT(DISTINCT AC.ID) n_activos, COUNT(DISTINCT INAC.ID) n_inactivos
FROM USUARIO AC, USUARIO INAC
WHERE AC.activo = 1 AND INAC.activo = 0*/

--3 usuarios de menor altura ordenados por edad
/*
select *
from (select top(3) * from [dbo].[Usuario] order by altura asc) as subc
order by edad*/

-- devuelve los usuarios con el mismo coche

/*select *
from Usuario as usuario1 Join Usuario as usuario2 on usuario1.coche = usuario2.coche
where  usuario1.Id<usuario2.Id*/

--usuario de mas edad con coche

/*
select top(1) *
from [dbo].[Usuario]
where coche is not null
order by edad desc
*/

--usuario de menos edad inactivo y sin coche
/*select top(1) *
from [dbo].[Usuario]
where coche is null and activo =0
order by edad asc*/

--calcula la media de edades de los usuarios activos, inactivos, con coche y sin coche

select avg(edad) as media_edad_activos
from Usuario
where activo =1

select avg(edad) as media_edad_inactivos
from Usuario
where activo =0

select avg(edad) as media_edad_sin_coche
from Usuario
where coche is null

select avg(edad) as media_edad_con_coche
from Usuario
where coche is not null
