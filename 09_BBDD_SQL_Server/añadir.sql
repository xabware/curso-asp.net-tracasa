﻿DECLARE @count as int
declare @usu as int
declare @prod as int
declare @fecha as datetime
declare @cantidad as int

DECLARE @fechaini DATE = '2018-01-01'
DECLARE @fechafin DATE = '2021-09-30'

SET @count = 0
while @count < 300
begin
	set @count = @count+1
	set @usu = 2 + rand(CHECKSUM(newid())) *10
	set @prod = 1 + rand(CHECKSUM(newid())) *5
	set @cantidad = 1 + rand(CHECKSUM(newid())) *100
	set @fecha = DATEADD(DAY,ABS(CHECKSUM(NEWID()))% (1+DATEDIFF(DAY,@fechafin,@fechaini)),@fechaini)
	PRINT CAST(@fecha AS VARCHAR)
	print 'idUsu: ' + cast(@usu as varchar) + '-' + cast(@prod as varchar)
	INSERT INTO CompraUsuProd (idUsuario, idProducto, fecha, cantidad)
	VALUES (@usu,@prod,@fecha,@cantidad)
end;
