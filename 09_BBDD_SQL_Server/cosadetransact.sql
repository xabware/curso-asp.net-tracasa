﻿declare @aux_id INT
declare cur_u CURSOR FOR select u.id from Usuario U

OPEN cur_u
-- FOREACH cur_u -> @aux_id IN Usuario.Id
FETCH NEXT FROM cur_u INTO @aux_id

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT SUM(C.Cantidad), (SELECT Nombre FROM Usuario WHERE Usuario.id = @aux_id)
		FROM CompraUsuProd C WHERE C.idUsuario = @aux_id

	FETCH NEXT FROM cur_u INTO @aux_id
END

CLOSE cur_u
DEALLOCATE cur_u