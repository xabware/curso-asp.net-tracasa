import { Injectable } from "@angular/core"; 
import { HttpClient } from '@angular/common/http'
import { WeatherForecast } from "./weather-forecast";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root'
})

export class ServicioEjemploService {
    private activo: boolean;
    private arrayClima: Array<any> = [];
    private url:string = "http://localhost:41413/WeatherForecast"

    constructor(private clienteHTTP: HttpClient){
        this.activo = false;
        
        let observ: Observable<Array<WeatherForecast>>;
        observ = this.clienteHTTP.get<Array<WeatherForecast>>(this.url);
        observ.subscribe((datos:Array<WeatherForecast>) =>{
            this.arrayClima = datos;
        });
    }

    public activar(activo:boolean): void{
        this.activo = activo;
    }

    public siActivado(): boolean{
        return this.activo;
    }

    public climaArray(){
        return this.arrayClima;
    }
}