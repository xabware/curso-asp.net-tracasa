import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  template: `<p>trozo-web funciona!</p>
  <p>Mostrando valor propiedad con interpolación (variable interpolada)</p>
  <!--Property binding: Equivalente al \${expresion}-->
  {{"" + (10+5)+propiedadClase+" - "+ contadorComp }}
  <!--Event binding: Con los paréntesis vinculamos un evento HTML con un método TS de clase-->
  <input type="button" value="Aumentar" (click)="alPulsarBoton()"/>
  <p>Pulsado {{contadorComp}} veces</p>
  <!-- Double data-binding: Los dos anteriores en uno -->
  <span>Cambia el valor: </span><input type="number" [(ngModel)]="contadorComp">
  <h2>Directivas estructurales:</h2>
  <!-- Empiezan por *ngNombreDirectiva
      La programacion estructurada llevada al html declarado-->
  
  <ol>
      <li *ngFor="let comida of arrayDeTextos"><!-- Como un foreach-->
          He probado {{comida}}
      </li>
  </ol>
  <div *ngIf="contadorComp % 2 === 0">El contador es par</div>
  <div *ngIf="contadorComp % 2 === 1">El contador es impar</div>
  <!-- Crear componente directivas-struct y meter los dos ejemplos en él, y sustituir el tercer <trozo-web> por este nuevo componente-->`,
})
export class BindingsComponent implements OnInit {

  propiedadClase: string;
  static contadorEstatico: number = 0;
  contadorComp = 0;
  arrayDeTextos: Array<string>;

  constructor() {
    this.propiedadClase = "...";
    this.contadorComp = 1;
    this.arrayDeTextos = ["relleno","cuajada","tarta de cuajada","bacalao ajoarriero"];
   }

  ngOnInit(): void {
    this.propiedadClase = `ngOnInit es el primer método de ciclo de vida del componente (ejecutado: ${BindingsComponent.contadorEstatico} veces)`;
  }

  alPulsarBoton(): void{
    this.contadorComp ++;
  }

}
