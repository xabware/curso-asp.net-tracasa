import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas-struct',
  templateUrl: './directivas-struct.component.html',
  styleUrls: ['./directivas-struct.component.css']
})
export class DirectivasStructComponent implements OnInit {

  propiedadClase: string;
  arrayDeTextos: Array<string>;
  contadorComp = 0;
  
  constructor() {
    this.propiedadClase = "...";
    this.arrayDeTextos=["a","b","c","d"];
    this.contadorComp = 1;
   }

  ngOnInit(): void {
  }

  alPulsarBoton(): void{
    this.contadorComp ++;
  }

}
