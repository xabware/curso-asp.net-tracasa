import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivasStructComponent } from './directivas-struct.component';

describe('DirectivasStructComponent', () => {
  let component: DirectivasStructComponent;
  let fixture: ComponentFixture<DirectivasStructComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectivasStructComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivasStructComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
