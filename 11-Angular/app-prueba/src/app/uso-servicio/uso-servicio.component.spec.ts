import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsoServicioComponent } from './uso-servicio.component';

describe('UsoServicioComponent', () => {
  let component: UsoServicioComponent;
  let fixture: ComponentFixture<UsoServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsoServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsoServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
