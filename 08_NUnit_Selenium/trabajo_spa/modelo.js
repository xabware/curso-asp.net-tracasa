class Modelo {
    constructor() {
        this.actualizar();
    }

    actualizar() {
        this.usuarios = [];
        this.cargarUsuarios();
    }

    borrarUsuario(nombre) {
        var confirmacion = confirm("¿Seguro que quieres eliminar el usuario " + nombre + "?");
        if (!confirmacion) {
            return;
        }

        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === nombre) {
                this.usuarios.splice(i, 1);
                break;
            }
        }

        this.guardarUsuarios();
    }

    borrarTodo() {
        var confirmacion = confirm("¿Seguro que quieres eliminar todo?");
        if (!confirmacion) {
            return;
        }

        this.usuarios.splice(0, this.usuarios.length);
        this.guardarUsuarios();
    }

    cancelar() {
        this.editar = [];
        this.guardarUsuarios();
    }

    modificarUsuario(nuevoNombre, edad, altura, activo) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === this.editar["nombre"]) {
                this.usuarios[i]["nombre"] = nuevoNombre;
                this.usuarios[i]["edad"] = edad;
                this.usuarios[i]["altura"] = altura;
                this.usuarios[i]["activo"] = activo;
                this.editar = [];
                this.guardarUsuarios();
                return;
            }
        }
        this.actualizar();
        alert("No encontrado: " + this.editar["nombre"]);
    }

    cargarModificar(nombre) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"] === nombre) {
                this.editar = this.usuarios[i];
                this.guardarUsuarios();
                return;
            }
        }
        alert("No encontrado");
    }

    buscarNombre(nombre) {
        for (let i in this.usuarios) {
            if (this.usuarios[i]["nombre"].toUpperCase() === nombre.toUpperCase()) {
                return true;
            }
        }
        return false;
    }

    getUsuarios() {
        return this.usuarios;
    }

    crearUsuario(nombre, edad, altura, activo) {
        this.usuarios.push(new Usuario(nombre, edad, altura, activo));
    }

    guardarUsuarios() {
        let jsonUsuarios = {
            "usuarios": this.usuarios,
            "editar": this.editar
        }
        window.localStorage.setItem("datos", JSON.stringify(jsonUsuarios));
    }

    cargarUsuarios() {
        let datos = JSON.parse(window.localStorage.getItem("datos"));
        let tmpUsuarios;
        try {
            tmpUsuarios = datos["usuarios"];
        } catch (error) {
            tmpUsuarios = [];
        }

        try {
            this.editar = datos["editar"];
        } catch (error) {
            this.editar = [];
        }

        for (let i in tmpUsuarios) {
            this.crearUsuario(tmpUsuarios[i]["nombre"], tmpUsuarios[i]["edad"], tmpUsuarios[i]["altura"], tmpUsuarios[i]["activo"]);
        }
        console.log(this.usuarios);
    }
}