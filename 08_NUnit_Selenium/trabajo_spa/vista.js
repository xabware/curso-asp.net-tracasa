class Vista{
    constructor(usuarios, editar) {
        this.cargarTabla(usuarios);
    }

    actualizar(usuarios, editar){
        this.cargarTabla(usuarios);
        this.seleccionarBoton(editar);
    }

    seleccionarBoton(editar){
        if (editar.length == 0) {
            console.log("añadiendo");
            document.getElementsByTagName("input")[0].value = "";
            document.getElementsByTagName("input")[3].checked = "";
            document.getElementsByTagName("input")[2].value = "";
            document.getElementsByTagName("input")[1].value = "";
            document.getElementsByTagName('input')[4].style.visibility = 'visible';
            document.getElementsByTagName('input')[5].style.visibility = 'hidden';
            document.getElementsByTagName('input')[6].style.visibility = 'hidden';
            document.getElementsByTagName('legend')[0].innerHTML = "Añadir usuario";
        } else {
            console.log("editando");
            console.log(editar);
            document.getElementsByTagName("input")[0].value = editar["nombre"];
            document.getElementsByTagName("input")[3].checked = editar["activo"];
            document.getElementsByTagName("input")[1].value = editar["edad"];
            document.getElementsByTagName("input")[2].value = editar["altura"];
            document.getElementsByTagName('input')[4].style.visibility = 'hidden';
            console.log(document.getElementsByTagName('input')[5])
            document.getElementsByTagName('input')[5].style.visibility = 'visible';
            document.getElementsByTagName('input')[6].style.visibility = 'visible';
            document.getElementsByTagName('legend')[0].innerHTML = "Modificar usuario - " + editar["nombre"];
        }
    }

    cargarTabla(usuarios) {
        var tbodyRef = document.getElementById('tabla-usuarios').getElementsByTagName('tbody')[0];
        tbodyRef.innerHTML = "";
        console.log("Actualizando");

        for (let indice in usuarios) {
            this.anadirFila(usuarios[indice]["nombre"], usuarios[indice]["edad"], usuarios[indice]["altura"], usuarios[indice]["activo"], tbodyRef);
        }
    }

    mensajeError(msg) {
        var field = document.getElementsByTagName('fieldset')[1];
        var field2 = field.getElementsByTagName("p")[0];
        field.style.visibility = 'visible';
        field2.innerHTML = msg;
    }

    ocultarMensaje() {
        document.getElementsByTagName('fieldset')[1].style.visibility = 'hidden';
    }

    anadirFila(nombre, edad, altura, activo, tbody){
        // Insert a row at the end of table
        var newRow = tbody.insertRow();

        // Insert a cell at the end of the row
        var newCell = newRow.insertCell();
        var newText = document.createTextNode(nombre);
        newCell.appendChild(newText);

        // Append a text node to the cell
        var newCell = newRow.insertCell();
        var newText = document.createTextNode(edad);
        newCell.appendChild(newText);

        // Append a text node to the cell
        var newCell = newRow.insertCell();
        var newText = document.createTextNode(altura);
        newCell.appendChild(newText);

        // Append a text node to the cell
        var checkBox = document.createElement("INPUT");
        checkBox.type = "checkbox";
        checkBox.checked = activo;
        checkBox.disabled = "disabled";

        var newCell = newRow.insertCell();
        newCell.appendChild(checkBox);
  
        var cellBtnBorrar = newRow.insertCell();
        var btnBorrar = document.createElement("BUTTON");
        btnBorrar.innerHTML = "Borrar";
        btnBorrar.addEventListener("click", () => {
            this.borrarUsuarioUnico(nombre);
        });
        cellBtnBorrar.appendChild(btnBorrar);

        var cellBtnEditar = newRow.insertCell();
        var btnEditar = document.createElement("BUTTON");
        btnEditar.innerHTML = "Editar"; 
        btnEditar.addEventListener("click", () => {
            this.cargarModificarUnico(nombre);
        });
        cellBtnEditar.appendChild(btnEditar);
    }
}
