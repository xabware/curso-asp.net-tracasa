class Controlador {
    constructor() {
        this.reNombre = /^[a-zA-Z\ ]{1,20}$/;
        this.reEdad = /[0-9]{1,}/;
        this.reAltura = /[0-9]{1,}/;

        this.modelo = new Modelo();
        this.vista = new Vista(this.modelo.usuarios, this.modelo.editar);

        this.vista.borrarUsuarioUnico = (nombre) => {
            this.borrarUsuario(nombre);
        }

        this.vista.cargarModificarUnico = (nombre) => {
            this.cargarModificar(nombre);
        }
    }

    crearUsuario() {
        let nombre = document.getElementById("nombre").value;
        if (!this.reNombre.test(nombre)) {
            this.vista.mensajeError("Formato nombre no valido");
            return;
        }
        let edad = document.getElementById("edad").value;
        if (edad > 120 || edad < 1 || !this.reEdad.test(edad)) {
            this.vista.mensajeError("Edad no valida");
            return;
        }
        let altura = document.getElementById("altura").value;
        if (altura > 300 || altura < 1 || !this.reAltura.test(altura)) {
            this.vista.mensajeError("Altura no valida");
            return;
        }
        let activo = document.getElementById("activo").checked;
        if (!this.modelo.buscarNombre(nombre)) {
            this.modelo.crearUsuario(nombre, edad, altura, activo);
            this.modelo.guardarUsuarios();
            this.modelo.cargarUsuarios();
            this.modelo.actualizar()
            this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
            this.vista.mensajeError("Usuario añadido");
        } else {
            this.vista.mensajeError("Usuario repetido");
        }
    }

    cargarModificar(nombre) {
        this.modelo.cargarModificar(nombre);
        this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
    }

    modificarUsuario() {
        let nombre = document.getElementById("nombre").value;
        if (!this.reNombre.test(nombre)) {
            this.vista.mensajeError("El nombre no puede ser vacio");
            return;
        }
        let edad = document.getElementById("edad").value;
        if (edad > 120 || edad < 1 || !this.reEdad.test(edad)) {
            this.vista.mensajeError("Edad no valida");
            return;
        }
        let altura = document.getElementById("altura").value;
        if (altura > 300 || altura < 1 || !this.reAltura.test(altura)) {
            this.vista.mensajeError("Altura no valida");
            return;
        }
        let activo = document.getElementById("activo").checked;
        this.modelo.modificarUsuario(nombre, edad, altura, activo);
        this.modelo.actualizar();
        this.vista.seleccionarBoton(this.modelo.editar);
        this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
        vista.mensajeError("Usuario modificado");
    }

    cargarBotones() {
        console.log(this.modelo.editar);
        this.vista.seleccionarBoton(this.modelo.editar);
    }

    cancelar() {
        this.modelo.cancelar();
        this.modelo.actualizar();
        this.vista.ocultarMensaje();
        this.vista.seleccionarBoton(this.modelo.editar);
        this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
    }

    borrarUsuario(nombre) {
        this.modelo.borrarUsuario(nombre);
        this.vista.mensajeError("Usuario borrado");
        this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
    }

    borrarTodo() {
        this.modelo.borrarTodo();
        this.vista.mensajeError("Borrado todo");
        this.vista.actualizar(this.modelo.usuarios, this.modelo.editar);
    }
}

window.addEventListener("load", function () {
    controlador = new Controlador();

    document.getElementById("btn-anadir").addEventListener("click", () => {
        controlador.crearUsuario();
    });

    document.getElementById("btn-modificar").addEventListener("click", () => {
        controlador.modificarUsuario();
    });

    document.getElementById("btn-cancelar").addEventListener("click", () => {
        controlador.cancelar();
    });

    document.getElementById("btn-borrar-todo").addEventListener("click", () => {
        controlador.borrarTodo();
    });
});