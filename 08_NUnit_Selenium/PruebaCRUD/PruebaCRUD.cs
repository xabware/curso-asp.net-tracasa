using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PruebaCRUD
{
    public class PruebaCRUD
    {
        private static List<string> listaNombresBuenos = new List<string>() { "Aitor Tilla", "Armando Bulla", "Elsa pito", "Debora Teste", "Elvis Nieto", "Rosa Melano", "Elton Tito", "Elsa Capunta", "Elvis Tek", "Lucho Portuano" };
        static string rutaSpa;
        FirefoxDriver driver;
        IWebElement nombre, edad, altura, activo, btnAnadir, btnModificar, btnCancelar;
        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            rutaSpa = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\trabajo_spa\\index.html";
            string fichFirefox = "..\\..\\..\\..\\FirefoxPortable\\App\\Firefox64\\firefox.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "..\\..\\..\\..\\FirefoxPortable_92.0_English.paf.exe";
                if (File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if (File.Exists(fichFirefox))
            {
                FirefoxDriverService geckoService = FirefoxDriverService.CreateDefaultService("./");
                geckoService.Host = "::1";
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = fichFirefox;
                firefoxOptions.AcceptInsecureCertificates = true;

                driver = new FirefoxDriver(geckoService, firefoxOptions);
            }
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            driver.Close();
        }

        [TearDown]
        public void tearDown()
        {
            var botonesEliminar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[5]"));
            while (botonesEliminar.Count > 0)
            {
                botonesEliminar[0].Click();
                driver.SwitchTo().Alert().Accept();
                botonesEliminar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[5]"));
            }
        }

        [SetUp]
        public void setup()
        {
            driver.Navigate().GoToUrl(rutaSpa);

            nombre = driver.FindElement(By.Id("nombre"));
            edad = driver.FindElement(By.Id("edad"));
            altura = driver.FindElement(By.Id("altura"));
            activo = driver.FindElement(By.Id("activo"));
            btnAnadir = driver.FindElement(By.Id("btn-anadir"));
            btnModificar = driver.FindElement(By.Id("btn-modificar"));
            btnCancelar = driver.FindElement(By.Id("btn-cancelar"));
        }

        //Teses
        [Test]
        public void CrearUsuarios()
        {
            int tOriginal = TableSize();
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);
            HacerAlgo(listaNombres[0], 24, 240, true, btnAnadir);
            AssertExisteUsuario(listaNombres[0], 24, 240, true);

            HacerAlgo(listaNombres[1], 99, 200, false, btnAnadir);
            AssertExisteUsuario(listaNombres[1], 99, 200, false);

            HacerAlgo(listaNombres[2], 999, 999, false, btnAnadir);
            AssertNoExisteUsuario(listaNombres[2], 999, 999, false);

            HacerAlgo(listaNombres[3], 99, 99, false, btnAnadir);
            AssertExisteUsuario(listaNombres[3], 99, 99, false);

            HacerAlgo(listaNombres[4], 10, 20, true, btnAnadir);
            AssertExisteUsuario(listaNombres[4], 10, 20, true);

            HacerAlgo(listaNombres[2], 55, 30, false, btnAnadir);
            AssertExisteUsuario(listaNombres[2], 55, 30, false);

            Assert.AreEqual(TableSize(), tOriginal + 5, "el tama�o de la tabla es incorrecto");

            driver.Navigate().Refresh();
            AssertExisteUsuario(listaNombres[0], 24, 240, true);
            AssertExisteUsuario(listaNombres[1], 99, 200, false);
            AssertNoExisteUsuario(listaNombres[2], 999, 999, false);
            AssertExisteUsuario(listaNombres[3], 99, 99, false);
            AssertExisteUsuario(listaNombres[4], 10, 20, true);
            AssertExisteUsuario(listaNombres[2], 55, 30, false);
            Assert.AreEqual(TableSize(), tOriginal + 5, "el tama�o de la tabla es incorrecto");
        }

        [Test]
        public void Editar()
        {
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(1);
            int tOriginal = TableSize();

            HacerAlgo(listaNombres[0], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[0], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(listaNombres[0], listaNombres[0] + " mod", 22, 177, true, true);
            AssertExisteUsuario(listaNombres[0] + " mod", 22, 177, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(listaNombres[0] + " mod", listaNombres[0] + " mod", 22, 130, true, true);
            AssertExisteUsuario(listaNombres[0] + " mod", 22, 130, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(listaNombres[0] + " mod", listaNombres[0] + " mod", 80, 80, true, true);
            AssertExisteUsuario(listaNombres[0] + " mod", 80, 80, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            editarUsuario(listaNombres[0] + " mod", listaNombres[0] + " mod", 22, 177, true, false);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");
        }

        [Test]
        public void Eliminar()
        {
            int tOriginal = TableSize();
            List<string> listaNombres = listaNombresBuenos.GetRandomElements<string>(5);

            HacerAlgo(listaNombres[0], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[0], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            HacerAlgo(listaNombres[1], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[1], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 2, "el tama�o de la tabla es incorrecto");

            HacerAlgo(listaNombres[2], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[2], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 3, "el tama�o de la tabla es incorrecto");

            HacerAlgo(listaNombres[3], 21, 180, true, btnAnadir);
            AssertExisteUsuario(listaNombres[3], 21, 180, true);
            Assert.AreEqual(TableSize(), tOriginal + 4, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaNombres[0], false);
            Assert.AreEqual(TableSize(), tOriginal + 4, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaNombres[0], true);
            Assert.AreEqual(TableSize(), tOriginal + 3, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaNombres[1], true);
            Assert.AreEqual(TableSize(), tOriginal + 2, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaNombres[2], true);
            Assert.AreEqual(TableSize(), tOriginal + 1, "el tama�o de la tabla es incorrecto");

            eliminarUsuario(listaNombres[3], true);
            Assert.AreEqual(TableSize(), tOriginal, "el tama�o de la tabla es incorrecto");
        }

        //Funciones
        public void editarUsuario(string NombreAntiguo, string NombreNuevo, int intEdad, int intAltura, bool boolActivo, bool boolModif)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var botonesEditar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[6]"));

            int i = 0;
            foreach (var nombre in nombresTabla)
            {
                if (nombre.Text == NombreAntiguo)
                {
                    botonesEditar[i].Click();
                    break;
                }
                i++;
            }
            if (i > nombresTabla.Count)
            {
                Assert.Fail("No se ha encontrado el usuario urbez");
            }

            if (boolModif)
            {
                HacerAlgo(NombreNuevo, intEdad, intAltura, boolActivo, btnModificar);
            } else
            {
                btnCancelar.Click();
            }

        }

        public void eliminarUsuario(string nombreUsuarioEliminar, bool eliminar)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var botonesEliminar = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[5]"));

            int i = 0;
            foreach (var nombre in nombresTabla)
            {
                if (nombre.Text == nombreUsuarioEliminar)
                {
                    botonesEliminar[i].Click();
                    break;
                }
                i++;
            }

            if (eliminar) {
                driver.SwitchTo().Alert().Accept();
            }
            else
            {
                driver.SwitchTo().Alert().Dismiss();
            }
        }

        private void HacerAlgo(string strNombre, int intEdad, int intAltura, bool boolActivo, IWebElement btn)
        {
            limpiarCampos();
            nombre.SendKeys(strNombre);
            edad.SendKeys(Keys.Right + Keys.Backspace + intEdad);
            altura.SendKeys(Keys.Right + Keys.Backspace + intAltura);
            if (boolActivo)
            {
                activo.Click();
            }
            btn.Click();
        }

        private int TableSize()
        {
            return driver.FindElements(By.XPath("/html/body/table/tbody/tr")).Count;
        }

        private void AssertExisteUsuario(string nombreUsuario, int edadUsuario, int alturaUsuario, bool isActivoUsuario)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var edadesTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[2]"));
            var alturasTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[3]"));
            var activosTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[4]/input"));
            int posicion = -1;
            for (int i = 0; i < nombresTabla.Count; i++)
            {
                if (nombresTabla[i].Text == nombreUsuario &&
                   edadesTabla[i].Text == edadUsuario.ToString() &&
                   alturasTabla[i].Text == alturaUsuario.ToString() &&
                   bool.Parse(activosTabla[i].GetProperty("checked")) == isActivoUsuario)
                {
                    posicion = i;
                    return;
                }
            }
            Assert.Fail("No se ha encontrado el usuario");
        }

        private void AssertNoExisteUsuario(string nombreUsuario, int edadUsuario, int alturaUsuario, bool isActivoUsuario)
        {
            var nombresTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[1]"));
            var edadesTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[2]"));
            var alturasTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[3]"));
            var activosTabla = driver.FindElements(By.XPath("/html/body/table/tbody/tr/td[4]/input"));
            for (int i = 0; i < nombresTabla.Count; i++)
            {
                if (nombresTabla[i].Text == nombreUsuario &&
                   edadesTabla[i].Text == edadUsuario.ToString() &&
                   alturasTabla[i].Text == alturaUsuario.ToString() &&
                   bool.Parse(activosTabla[i].GetProperty("checked")) == isActivoUsuario)
                {
                    Assert.Fail("No se ha encontrado el usuario");
                }
            }
        }

        private void limpiarCampos()
        {
            nombre.Clear();
            edad.Clear();
            altura.Clear();
            if (activo.Selected)
            {
                activo.Click();
            }
        }        
    }

    public static class extension
    {
        public static List<T> GetRandomElements<T>(this IEnumerable<T> list, int elementsCount)
        {
            return list.OrderBy(arg => Guid.NewGuid()).Take(elementsCount).ToList();
        }
    }
    
}
