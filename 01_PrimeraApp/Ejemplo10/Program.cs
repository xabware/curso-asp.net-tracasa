﻿using System;

namespace Ejemplo10
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Escribir un programa que pida ingresar coordenadas (x,y) que representan puntos en el plano.
            Informar cuántos puntos se han ingresado en el primer, segundo, tercer y cuarto cuadrante. 
            Al comenzar el programa se pide que se ingrese la cantidad de puntos a procesar.*/
            int x, y, c=0;
            int pri=0, sec=0, ter=0, cua=0;
            string linea;
            bool noPos=true;
            bool error=false;
            while (noPos || error)
            {
                Console.Write("Ingrese el numero de puntos: ");
                linea = Console.ReadLine();
                error = !int.TryParse(linea, out c);
                noPos = c <= 0;
                if (noPos || error)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Tiene que ser un número entero positivo.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            for (int i = 0; i < c; i++)
            {
                string lineax, lineay;
                bool flag = false;
                Console.WriteLine("Punto número " + (i+1) + ":");
                do
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error al introducir el punto.");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    flag = true;
                    Console.Write("Introduce la coordenada x: ");
                    lineax = Console.ReadLine();
                    Console.Write("Introduce la coordenada y: ");
                    lineay = Console.ReadLine();
                } while (!int.TryParse(lineax, out x) || !int.TryParse(lineay,out y));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Punto introducido correctamente.");
                Console.ForegroundColor = ConsoleColor.White;
                uint ux = (uint)x;
                uint uy = (uint)y;
                uint cuadrante= ((ux >> 30) &2) | (uy >> 31);
                switch (cuadrante)
                {
                    case 0:
                        pri += 1;
                        break;
                    case 1:
                        sec += 1;
                        break;
                    case 2:
                        cua += 1;
                        break;
                    default:
                        ter += 1;
                        break;
                }
            }
            Console.WriteLine("----------------------------------------------------");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("En el primer cuadrante hay: " + pri + " puntos.");
            Console.WriteLine("En el segundo cuadrante hay: " + sec + " puntos.");
            Console.WriteLine("En el tercer cuadrante hay: " + ter + " puntos.");
            Console.WriteLine("En el cuarto cuadrante hay: " + cua + " puntos.");
            Console.ForegroundColor = ConsoleColor.White;

        }
    }
}
