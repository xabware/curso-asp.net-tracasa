﻿using System;

namespace valor_referencia
{
    class Program
    {
        static void Main(string[] args)
        {
            string cadena = "hey wassup";
            Console.WriteLine("string fuera y antes: " + cadena);
            ToUpper(ref cadena);
            Console.WriteLine("string fuera y después: " + cadena);
        }

        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: "+entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y después: "+entero);
        }
        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes: "+entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y después: "+entero);
        }

        static void ToUpper(ref string cadena)
        {
            cadena = cadena.ToUpper();
            cadena = string.Concat(cadena, "!!");
        }


    }
}
