﻿using System;

namespace Ejemplo8
{
    class Program
    {
        static void Main(string[] args)
        {
            int c=0, max=int.MinValue, min=int.MaxValue, num;
            bool noPos=true;
            string linea;
            while (noPos)
            {
                Console.Write("Ingrese el numero de valores:");
                linea = Console.ReadLine();
                c = int.Parse(linea);
                noPos = c <= 0;
                if (noPos)
                {
                    Console.WriteLine("Tiene que ser un número entero positivo.");
                }
            }
            for (int i = 0; i < c; i++)
            {
                bool flag = false;
                do
                {
                    if (flag)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error al introducir el valor.");
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    flag = true;
                    Console.Write("Ingrese un valor:");
                    linea = Console.ReadLine();
                } while (!int.TryParse(linea,out num));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Valor introducido correctamente.");
                Console.ForegroundColor = ConsoleColor.White;
                if (num > max)
                {
                    max = num;
                }
                else
                {
                    if (num < min)
                    {
                        min = num;
                    }
                }
            }
            Console.WriteLine("El máximo es: " + max);
            Console.WriteLine("El mínimo es: " + min);
        }
            
    }
}
