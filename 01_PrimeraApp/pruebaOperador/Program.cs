﻿using System;

namespace pruebaOperador
{
    class Program
    {
        static void Main(string[] args)
        {
            string a, b;
            a = null;
            b = null;
            a = "Gato";
            b = "Perro";
            Console.WriteLine(a ?? b);
            Console.WriteLine(b ?? a);
            Console.ReadLine();
        }
    }
}
