﻿using System;

namespace strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] texto= new string[3];
            for (int i = 0; i < texto.Length; i++)
            {
                Console.Write("Escribe una palabra: ");
                texto[i] = String.Join(" ", Console.ReadLine().Split(" ", StringSplitOptions.RemoveEmptyEntries));
            }
            Console.WriteLine(String.Join(",", texto));
        }
    }
}
