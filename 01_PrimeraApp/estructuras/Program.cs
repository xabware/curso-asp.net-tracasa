﻿using System;

namespace estructuras
{
    class Program
    {
        struct Usuario
        {
            public string nombre;
            public int edad;
            public double altura;

            public Usuario (string nombre, int edad, double altura)
            {
                this.nombre = nombre;
                this.edad = edad;
                this.altura = altura;
            }

            public void MostrarUsuario()
            {
                Console.WriteLine("El usuario se llama " + this.nombre + ", Tiene " + this.edad + " años, y mide " + this.altura + "m.");
            }

        }

        static void Main(string[] args)
        {
            /*Crear estructura Usuario con nombre(texto), edad(entero) y altura(decimal)
              Crear dos variables usuarios
              crear un array de usuarios con 4 usuarios, los dos de antes y 2 mas que pidamos por teclado
              crear un método MostrarUsuario que muestre sus datos. Al terminar mostrar los 4 usuarios
             */

            Usuario user1 = new Usuario("El pepe", 21, 1.91);
            Usuario user2 = new Usuario("Ete sech", 99, 1.78);

            Usuario[] lista = new Usuario[4];
            lista[0] = user1;
            lista[1] = user2;
            for(int i = 2; i < 4; i++)
            {
                string linea;
                int edad;
                double altura;
                Console.Write("Escribe el nombre del usuario: ");
                string nombre = Console.ReadLine();
                Console.Write("Escribe la edad del usuario: ");
                linea = Console.ReadLine();
                edad = int.Parse(linea);
                Console.Write("Escribe la altura del usuario: ");
                linea = Console.ReadLine();
                altura = double.Parse(linea);
                lista[i] = new Usuario(nombre, edad, altura);
                Console.WriteLine("-------------------------------------");
            }

            for (int i = 0; i < 4; i++)
            {
                lista[i].MostrarUsuario();
            }
        }
    }
}
