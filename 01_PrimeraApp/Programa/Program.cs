﻿using System;

namespace Programa
{

    /*Crear una estructura productoE independiente (NO anidada)
             con nombre y precio, y su constructor
            con una función para mostrar sus datos
            
            Crear una clase ProductoC independiente (NO anidada)
            con nombre y precio, y su constructor
            con una funcion para mostrar sus datos

            Crear 4 funciones estáticas en Program:
                - Una que reciba ProductoE y modifique su nombre y su precio
                - Otra que reciba una red ProductoE y modifique su nombre y su precio
                - Una que reciba ProductoC y modifique su nombre y su precio
                - Otra que reciba una ref ProductoC y modifique su nombre y su precio.

            Por último, en main, comprobar el comportamiento para saber cual modifica realmente la variable original
             */

    class Program
    {
        /*  1 - Las estructuras siempre se pasan por valor a menos que se indique aposta
     *  con ref que es por referencia
     *  2 - No pueden heredar  unas estructuras de otras
     *  3 - No pueden haber estructuras estáticas
     *  4 - Siempre siempre tienen un constructor por defecto
     */
        struct ProductoE
        {
            public string nombre;
            public int precio;

            public ProductoE(string nombre, int precio)
            {
                this.nombre = nombre;
                this.precio = precio;
            }

            override public string ToString()
            {
                return ("El producto " + nombre + " tiene un valor de " + precio + " euros.");
            }
        }

        /*  1 - Las clases siempre se pasan por  referencia. Es redundante usar ref
  *  2 - Sí pueden heredar unas clases de otras
  *  3 - Sí pueden haber clases estáticas
  *  4 - Sólo tienen un constructor por defecto cuando no hemos 
  *       creado un contructor explícitamente con algún argumento
  */
        class ProductoC
        {
            public string nombre;
            public int precio;

            public ProductoC(string nombre, int precio)
            {
                this.nombre = nombre;
                this.precio = precio;
            }
            override public string ToString()
            {
                return ("El producto " + nombre + " tiene un valor de " + precio + " euros.");
            }
        }

        static void Main(string[] args)
        {
            ProductoC prodC = new ProductoC("CValor", 10);
            ProductoC prodCref = new ProductoC("CRef", 10);
            ProductoE prodE = new ProductoE("EValor", 10);
            ProductoE prodEref = new ProductoE("ERef", 10);
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Producto C por valor antes: "+prodC.ToString());
            cambiarC(prodC);
            Console.WriteLine("Producto C por valor después: " + prodC.ToString());
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Producto C por referencia antes: "+prodCref.ToString());
            cambiarCRef(ref prodCref);
            Console.WriteLine("Producto C por referencia después: " + prodCref.ToString());
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Producto E por valor antes: "+prodE.ToString());
            cambiarE(prodE);
            Console.WriteLine("Producto E por valor después: " + prodE.ToString());
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Producto E por referencia antes: "+prodEref.ToString());
            cambiarERef(ref prodEref);
            Console.WriteLine("Producto E por referencia después: " + prodEref.ToString());
            Console.WriteLine("---------------------------------");
        }

        static void cambiarE(ProductoE producto)
        {
            Console.WriteLine("Producto dentro y antes: " + producto.ToString());
            producto.precio = producto.precio * 2;
            producto.nombre = String.Concat("prod_", producto.nombre);
            Console.WriteLine("Producto dentro y después: " + producto.ToString());
        }
        static void cambiarERef(ref ProductoE producto)
        {
            Console.WriteLine("Producto dentro y antes: " + producto.ToString());
            producto.precio = producto.precio * 2;
            producto.nombre = String.Concat("prod_", producto.nombre);
            Console.WriteLine("Producto dentro y después: " + producto.ToString());
        }

        static void cambiarC(ProductoC producto)
        {
            Console.WriteLine("Producto dentro y antes: " + producto.ToString());
            producto.precio = producto.precio * 2;
            producto.nombre = String.Concat("prod_", producto.nombre);
            Console.WriteLine("Producto dentro y después: " + producto.ToString());
        }

        static void cambiarCRef(ref ProductoC producto)
        {
            Console.WriteLine("Producto dentro y antes: " + producto.ToString());
            producto.precio = producto.precio * 2;
            producto.nombre = String.Concat("prod_", producto.nombre);
            Console.WriteLine("Producto dentro y después: " + producto.ToString());
        }
    }
}
