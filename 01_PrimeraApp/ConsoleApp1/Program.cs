﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Console.ForegroundColor = ConsoleColor.Green;
            while (true)
            {
                Double ale = rand.NextDouble();
                if (ale < 0.4)
                {
                    Console.Write(0);
                }
                else if (ale<0.8)
                {
                    Console.Write(1);
                }
                else
                {
                    Console.Write(" ");
                }
            }


        }
    }
}
