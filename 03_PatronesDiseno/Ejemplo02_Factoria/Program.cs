﻿using System;
using System.Collections.Generic;

namespace Ejemplo02_Factoria
{
    // https://refactoring.guru/es/design-patterns/factory-method/csharp/example

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vamos a la fabrica!");
            FactoriaProducto fabrica = new FactoriaProducto();

            List<Producto> lista;
            lista = fabrica.CrearLista(1, 4);
            foreach (var item in lista)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }

            List<Producto> lista2 = fabrica.CrearLista(2);
            foreach (var item in lista2)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }

            List<Producto> lista3 = fabrica.CrearLista(3, 7);
            foreach (var item in lista3)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }

            List<Producto> lista4 = fabrica.CrearLista(new int[] { 2, 7, 4, 9 });
            foreach (var item in lista4)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }
        }
    }
}
