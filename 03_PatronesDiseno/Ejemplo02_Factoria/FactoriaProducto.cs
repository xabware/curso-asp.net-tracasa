﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejemplo02_Factoria
{
    class FactoriaProducto
    {
        /*public Producto Crear()
        {
            return new Producto();
        }*/

        public Producto Crear(int id)
        {
            string nombre;
            switch (id)
            {
                case 1: nombre = "Uno"; break;
                case 2: nombre = "Dos"; break;
                case 3: nombre = "Tres"; break;
                case 4: nombre = "Cuatro"; break;
                default: nombre = "Otro a saber"; break;
            }
            return new Producto(id, nombre);
        }


        public List<Producto> CrearLista(int finRango)
        {
            List<Producto> productos = new List<Producto>();
            List<int> ids = Enumerable.Range(1, finRango).ToList();
            foreach (int id in ids)
            {
                productos.Add(Crear(id));
            }
            return productos;
        }

        public List<Producto> CrearLista(int iniRango,int finRango)
        {
            List < Producto > productos = new List<Producto>();
            List<int> ids = Enumerable.Range(iniRango, finRango).ToList();
            foreach (int id in ids)
            {
                productos.Add(Crear(id));
            }
            return productos;
        }

        public List<Producto> CrearLista(int[] ids)
        {
            List<Producto> productos = new List<Producto>();
            foreach (int id in ids)
            {
                productos.Add(Crear(id));
            }
            return productos;
        }

        public Producto Crear(int id, string nombre)
        {
            return new Producto(id, nombre);
        }
    }
}
