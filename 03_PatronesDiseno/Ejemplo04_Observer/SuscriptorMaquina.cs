﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorMaquina : ISuscriptorObervador
    {

        string nombre;

        public SuscriptorMaquina(string nombre)
        {
            this.nombre = nombre;
        }

        public void ActualizarNotificacion(string noticia)
        {
            Console.Beep();
            Console.WriteLine("¡Beep beep boop boop!");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine(nombre + ": " + noticia);
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("fichero_maquinas.txt", nombre + ": "+noticia+"\n");
        }
    }
}
