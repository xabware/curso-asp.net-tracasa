﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not);


    class PeriodicoObservado
    {

        IList<ISuscriptorObervador> listaSuscriptores;
        public NoticiaCiencia nuevaNotCiencia;
        public PeriodicoObservado()
        {
            this.listaSuscriptores = new List<ISuscriptorObervador>();
        }

        public void noticiaCiencia(string laNoticia)
        {
            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacion(laNoticia);
            }
        }

        public void NotificarNoticia(string titular, DateTime fecha)
        {

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("¡EXTRA EXTRA!");
            Console.WriteLine(fecha.ToString() + " " + titular);
            Console.ForegroundColor = ConsoleColor.White;

            foreach (var suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacion(titular);
            }
        }

        public void AddSuscriptor(ISuscriptorObervador suscriptor)
        {
            listaSuscriptores.Add(suscriptor);
        }

        public void RemoveSuscriptor(ISuscriptorObervador suscriptor)
        {
            listaSuscriptores.Remove(suscriptor);
        }

        public void CerrarPeriódico()
        {
            NotificarNoticia("No tenemos dinero, se acabó la chusta esta de periódico", DateTime.Now);
            listaSuscriptores.Clear();
        }
    }
}
