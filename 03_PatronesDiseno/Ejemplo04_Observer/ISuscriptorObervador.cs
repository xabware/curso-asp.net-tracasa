﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    interface ISuscriptorObervador
    {

        public void ActualizarNotificacion(string noticia);

    }
}
