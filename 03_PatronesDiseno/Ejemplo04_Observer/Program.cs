﻿using System;
using System.Threading;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Patron observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();

            //1- Un suscriptor humano
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            //2- Ocurre una noticia
            alDia.NotificarNoticia("un meteorito roza la luna", DateTime.Now);
            Thread.Sleep(2000);
            //3- Otro susciptor humano
            SuscriptorHumano alba = new SuscriptorHumano("Alba");
            alDia.AddSuscriptor(alba);
            //4- Otro suscriptor maquina
            SuscriptorMaquina bot = new SuscriptorMaquina("Joaquín el robotín");
            alDia.AddSuscriptor(bot);
            //5- Ocurre otra noticia
            alDia.NotificarNoticia("Sube el ibex 35", DateTime.Now);
            Thread.Sleep(2000);
            //6- El otro humano se desuscribe porque dice mucha mentira
            alDia.RemoveSuscriptor(alba);
            //7- Ocurre la ultima noticia
            alDia.NotificarNoticia("Se conocen 68 miles de millones de dígitos de pi, pero los ingenieros seguirán usando 3,14", DateTime.Now);
            Thread.Sleep(2000);
            //8- El periodico cierra
            alDia.CerrarPeriódico();
        }
    }
}
