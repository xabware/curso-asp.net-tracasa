﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorHumano: ISuscriptorObervador
    {

        string nombre;

        public SuscriptorHumano(string nombre)
        {
            this.nombre = nombre;
        }

        public void ActualizarNotificacion(string noticia)
        {
            Console.Beep();
            Console.WriteLine("¡Nuevas noticias!");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(nombre + ": " + noticia);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
