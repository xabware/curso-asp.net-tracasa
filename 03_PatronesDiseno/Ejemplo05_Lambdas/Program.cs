﻿using System;

namespace Ejemplo05_Lambdas
{
    // Vamos a crear un nuevo tipo de dato, que en vez de 
    // almacenar información, almacene una funcion con determinada
    //estructura o firma.
    //La estructura es como la interfaz de una función, pero sin importar el nombre:
    //Los tipos de datos que recibe, y el tipo de dato que devuelve.
    //En C, eran punteros a funciones, en Java , hasta Java8, eran interfaces con una sola función.
    //En JS funciones flecha. En C# son los delegados

    delegate void FuncionQueRecibeInt(int x);

    class Program
    {
        static void Main(string[] args)
        {
            // LAMBDAS
            // Funciones estataticas: 
            FuncionEstatatica(5);
            OtraEstatica(7);

            FuncionQueRecibeInt funcion;
            funcion = FuncionEstatatica;
            funcion(200);

            OtroSistema(funcion);

            
        }
        static void OtroSistema(FuncionQueRecibeInt LlamarFuncionExterna)
        {   // Queremos recibir una funcion como parametro:
            // LlamarFuncionExterna por ejemplo, que reciba un int
            //int valor = 3;
            LlamarFuncionExterna(3);
        }
        static void FuncionEstatatica(int x)
        {
            Console.WriteLine("No necesito un objeto para ser llamada: ");
            Console.WriteLine("Param: " + x);
        }
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra Estatica: ");
        }
    }
}
