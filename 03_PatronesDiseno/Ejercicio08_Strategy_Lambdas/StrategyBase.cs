﻿using System;
namespace OtroNameSpace
{
    // Ya no se puede heredar de esta clase,  sino sobreescribir las funcionalidades
    // mediante delegados (variables de función, funciones estáticas y/o lambdas
    delegate void ExecuteFunc();
    delegate void MostrarNombreFunc();
    class StrategyBase
    {
        private string nombre;

        public StrategyBase(string mensaje) {
            nombre = mensaje + " " + GetType().Name + ".Execute()";
            MostrarNombre = () => Console.WriteLine(nombre);
        }

        public void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.Write("\n");
        }
        public ExecuteFunc execute;
        public MostrarNombreFunc MostrarNombre;

        public string Nombre { get => nombre;}
    }
}
