﻿using OtroNameSpace;
using System;

namespace Ejercicio08_Strategy_Lambdas
{
    // MainApp Test para aplicacion
    class MainApp
    {
        static void Main()
        {
            StrategyBase estrategia;

            estrategia = ConcretesStrategies.AddConcreteStrategyA("Eoooooo");
            estrategia.execute();

            estrategia = ConcretesStrategies.AddConcreteStrategyB("");
            estrategia.execute();

            estrategia = ConcretesStrategies.AddConcreteStrategyC("Nombre directo");
            estrategia.execute();
        }

    }
    static class ConcretesStrategies
    {
        public static StrategyBase AddConcreteStrategyA(string mensaje)
        {
            // Dinamicamente le damos la funcionalidad que antes hacíamos por herencia
            // Tanto public override void Execute()
            // como  public override void MostrarNombre()
            StrategyBase strObj = new StrategyBase(mensaje);
            string textoLocal = "Estrategia A, texto local";
            // Clousure: encapsular/proteger/guardar/almacenar una variable local
            // para que posteriormente pueda usarse en una funcón lambda.
            bool boolClausurado = true;
            // Aquí estamos "clausurando" la antigua función mostrarNombre, para usarse dentro de la
            // nueva función lamda, mostrarNombre. 
            MostrarNombreFunc base_MostrarNombre = strObj.MostrarNombre;

            strObj.MostrarNombre = () =>
            {
                strObj.RepetirChar('_', 30);
                base_MostrarNombre(); // Como antes hacíamos con base.MostrarNombre();
                strObj.RepetirChar('_', 30);
                // Simplemente por usarse en una función interna a otro bloque 
                // de código, la variable persiste en el tiempo, lo que dure la función
                Console.WriteLine("Usamos la var clausurada: " + boolClausurado);
                boolClausurado = false;
            };
            strObj.execute = () => {
                strObj.MostrarNombre();
                strObj.RepetirChar('-', 3);
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
                };

            return strObj;
        }
        public static StrategyBase AddConcreteStrategyB(string mensaje)
        {
            // Dinamicamente le damos la funcionalidad que antes hacíamos por herencia
            // Tanto public override void Execute()
            // como  public override void MostrarNombre()
            StrategyBase strObj = new StrategyBase(mensaje);
            string textoLocal = "Estrategia B, texto local";
            MostrarNombreFunc base_MostrarNombre = strObj.MostrarNombre;
            strObj.execute = () => {
                base_MostrarNombre();
                strObj.RepetirChar('+', 30);
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
            };

            return strObj;
        }
        public static StrategyBase AddConcreteStrategyC(string mensaje)
        {
            // Dinamicamente le damos la funcionalidad que antes hacíamos por herencia
            // Tanto public override void Execute()
            // como  public override void MostrarNombre()
            StrategyBase strObj = new StrategyBase(mensaje);
            string textoLocal = "Estrategia C, texto local";
            strObj.MostrarNombre = () => Console.WriteLine("Soy la estrategia C: " + strObj.Nombre);
            strObj.execute = () => {
                strObj.MostrarNombre();
                strObj.RepetirChar('-', 20);
                Console.WriteLine("Usando var local en lambda: " + textoLocal);
            };

            return strObj;
        }

        
    }


}
