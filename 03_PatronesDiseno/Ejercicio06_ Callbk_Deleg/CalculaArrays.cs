﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio06__Callbk_Deleg
{
    class CalculaArrays
    {
        static public float Sumar(float[] array)
        {
            return array.Sum();
        }
        static public float Multiplicar(float[] array)
        {
            return array.Aggregate(1f, (a, b) => a * b);
        }
        static public float Restar(float[] array)
        {
            return array.Aggregate(array[0]*2, (a, b) => a - b);
        }
        static public float Dividir(float[] array)
        {
            return array.Aggregate(array[0]* array[0], (a, b) => a / b);
        }
    }
}
