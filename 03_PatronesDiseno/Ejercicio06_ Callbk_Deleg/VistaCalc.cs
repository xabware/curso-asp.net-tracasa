﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ejercicio06__Callbk_Deleg
{
    class VistaCalc
    {
        public static float Operar(Func<float[], float> suma,Func<float[], float> resta,Func<float[], float> multiplicar,Func<float[], float> dividir)
        {
            Console.Write("Indique el número de operadores que va a introducir: ");
            bool bien;
            bien = int.TryParse(Console.ReadLine(),out int numOp);
            float[] numeros= new float[numOp];
            if (bien)
            {
                for (int i=0; i < numOp; i++)
                {
                    numeros[i] = Comprobador.IntroducirNumero<float>("Introduce un operador: ", "Error, tiene que ser un float.");
                }
                string operacion;
                Dictionary<string, Func<float[], float>> operaciones = new Dictionary<string, Func<float[], float>>();
                operaciones.Add("+",suma);
                operaciones.Add("-",resta);
                operaciones.Add("*",multiplicar);
                operaciones.Add("*",dividir);
                do
                {
                    if (!bien)
                    {
                        Comprobador.mensajeColor("Error, introduce uno de los símbolos + - * /.",ConsoleColor.Red);
                    }
                    Console.Write("Indique que operación va a realizar(+,-,*,/): ");
                    List<string> posiblesOperaciones = new List<string>() { "+", "-", "*", "/" };
                    operacion = Console.ReadLine();
                    bien = posiblesOperaciones.Contains(operacion);
                } while (!bien);
                return operaciones[operacion](numeros);
            }
            throw new Exception("Error en la introducción de datos");
        }
    }
}
