﻿using System;
using System.Collections.Generic;

// Ejercicio: Crear dos sistemas (clases independientes 
// que no se conocen entre sí): 
// La primera tendrá 4 funciones para sumar, rest, mult, div
// todo con float. Estas podrán realizar la operación sobre un array:
// Ej: {9, 7, 5, 3}  suma -> 24     resta ->  -6        mul -> 945
//     {42, 3, 2}   div -> (42 / 3) / 2 -> 7        suma -> 47
//      {3} ->   cualquier operador devuelve 3.
// No puede estar vacio el array
//  La segunda clase, le pide al usuario cuantos operandos habrá,
//  y los operandos uno a uno, la operación, y mostrará el resultado
// Si alguien quiere, en vez de eso que resuelva 3+22++11 o 7*3*11*2

namespace Ejercicio06__Callbk_Deleg
{
    delegate float FuncionOperador(float[] operadores);
    class Program
    {

        static void Main(string[] args)
        {
            /*float resultado = VistaCalc.Operar(CalculaArrays.Sumar,CalculaArrays.Restar,CalculaArrays.Multiplicar,CalculaArrays.Dividir);
            Console.WriteLine("El resultado es: "+ resultado);*/
            //Console.WriteLine(EvaluadorAritmetico.EvaluarExpresión("(345,7 - 3)+2,34 * 345 /(4-1)"));
            VistaEvaluador();

        }

        static private void VistaEvaluador()
        {
            List<string> histExp = new List<string>();
            List<float> histRes = new List<float>();
            float resultado;
            bool seguir = true;
            Console.WriteLine("INSTRUCCIONES");
            Console.WriteLine("Este evaluador de expresiones recibe expresiones aritméticas con:");
            Console.WriteLine("    Sumas            +");
            Console.WriteLine("    Restas           -");
            Console.WriteLine("    Multiplicaciones *");
            Console.WriteLine("    Divisiones       /");
            Console.WriteLine("    Parentesis       ()");
            Console.WriteLine("escriba \"cerrar\" para terminar la ejecución");
            Console.WriteLine("escriba \"hist\" para ver un histórico de los resultados");
            do
            {
                Console.Write("Introduzca su expresión a continuación: ");
                string linea = Console.ReadLine();
                linea = linea.Replace(" ","");
                if (linea == "cerrar")
                {
                    seguir = false;
                }else if (linea == "hist")
                {
                    Comprobador.mensajeColor("Históricos", ConsoleColor.Cyan);
                    for (int i = 0; i < histExp.Count; i++)
                    {
                        Comprobador.mensajeColor("    "+histExp[i]+" = "+histRes[i], ConsoleColor.Cyan);
                    }
                }
                else
                {
                    try
                    {
                        resultado = EvaluadorAritmetico.EvaluarExpresion(linea);
                        histRes.Add(resultado);
                        histExp.Add(linea);
                        Comprobador.mensajeColor("Resultado: " + resultado, ConsoleColor.Green);
                    }
                    catch (FormatException)
                    {
                        Comprobador.mensajeColor("Expresión mal escrita", ConsoleColor.Red);
                    }
                    
                }
            } while (seguir);

        }
    }
}
