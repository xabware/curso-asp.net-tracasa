﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Ejercicio06__Callbk_Deleg
{
    class EvaluadorAritmetico
    {

        private static Regex regex = new Regex(@"(?x)
                ^
                (?> (?<p> \( )* (?>-?\d+(?:\.\d+)?) (?<-p> \) )* )
                (?>(?:
                    [-+*/]
                    (?> (?<p> \( )* (?>-?\d+(?:\.\d+)?) (?<-p> \) )* )
                )*)
                (?(p)(?!))
                $
            ");

        private static List<char> posiblesOperaciones = new List<char>() { '+', '-', '*', '/', '(', ')' };

        public static float EvaluarExpresion(string expresion)
        {
            if (regex.IsMatch(expresion))
            {
                char[] exp = expresion.ToCharArray();

                Stack<float> pilaValores = new Stack<float>();
                Stack<char> pilaOperaciones = new Stack<char>();
                

                int pos = 0;
                while (pos <= exp.Length)
                {
                    if (pos == exp.Length || exp[pos] == ')')
                    {
                        ProcesarParentesisCierre(pilaValores, pilaOperaciones);
                        pos++;
                    }
                    else if (exp[pos] >= '0' && exp[pos] <= '9')
                    {
                        ProcesarValor(exp, ref pos, pilaValores,pilaOperaciones);
                    }
                    else if (posiblesOperaciones.Contains(exp[pos]))
                    {
                        ProcesarOperacion(exp[pos], pilaValores, pilaOperaciones);
                        pos++;
                    }
                    else
                    {
                        pos++;
                    }

                }

                //El resultado ha quedado en la pila de operadores
                return pilaValores.Pop();
            }
            else
            {
                throw new FormatException("Expresión mal escrita");
            }
        }

        //Recibe: la expresion, la posicion actual, y la pila de valores.
        //Devuelve: void
        //Mientras el siguiente caracter sea un número lee y va calculando el número,
        //cuando el siguiente ya no es un número, guarda el número leido en la pila de valores y sale.
        //La posicion se guarda porque es pasada por referencia
        private static void ProcesarValor(char[] exp,ref int pos, Stack<float> pilaValores, Stack<char> pilaOperaciones)
        {
            bool negativo=false;
            if (pilaOperaciones.Count > 0 && pilaOperaciones.Peek() == '-')
            {
                if (pilaValores.Count == 0)
                {
                    negativo = true;
                    pilaOperaciones.Pop();
                }
                else
                {
                    if (posiblesOperaciones.Contains(exp[pos - 2]))
                    {
                        negativo = true;
                        pilaOperaciones.Pop();
                    }
                }
            }
            float value = 0;
            bool dec = false;
            int numDec = 1;
            while (pos < exp.Length && ((exp[pos] >= '0' && exp[pos] <= '9')||(exp[pos] == ',')))
            {
                
                if (exp[pos] == ',')
                {
                    dec = true;
                    pos++;
                }
                else
                {
                    if (!dec)
                    {
                        value = 10 * value + (exp[pos++] - '0');
                    }
                    else
                    {
                        value = value + ((exp[pos++] - '0') * (float)Math.Pow(0.1f,numDec));
                        numDec++;
                    }
                    
                }
            }
            if (negativo)
            {
                value = -value;
            }
            pilaValores.Push(value);

        }

        private static void ProcesarOperacion(char operacion, Stack<float> pilaValores, Stack<char> pilaOperaciones)
        {

            while (pilaValores.Count > 1 && pilaOperaciones.Count > 0 && OperacionCausaEvaluacion(operacion, pilaOperaciones.Peek()))
            {
                EjecutarOperacion(pilaValores, pilaOperaciones);
            }

            pilaOperaciones.Push(operacion);
        }

        private static void ProcesarParentesisCierre(Stack<float> pilaValores,Stack<char> pilaOperaciones)
        {

            while (pilaOperaciones.Count > 0 && pilaOperaciones.Peek() != '(')
            {
                EjecutarOperacion(pilaValores, pilaOperaciones);
            }
            if (pilaOperaciones.Count > 0)
            {
                pilaOperaciones.Pop();
            }
            

        }

        private static bool OperacionCausaEvaluacion(char op, char opPrevio)
        {
            bool evaluar = false;

            switch (op)
            {
                case '+':
                case '-':
                    evaluar = (opPrevio != '(');
                    break;
                case '*':
                case '/':
                    evaluar = (opPrevio == '*' || opPrevio == '/');
                    break;
                case ')':
                    evaluar = true;
                    break;
            }

            return evaluar;
        }

        private static void EjecutarOperacion(Stack<float> pilaValores, Stack<char> pilaOperaciones)
        {
            float valorDerecho = pilaValores.Pop();
            float valorIzquierdo = pilaValores.Pop();
            char operacion = pilaOperaciones.Pop();

            float resultado = 0f;
            switch (operacion)
            {
                case '+':
                    resultado = valorIzquierdo + valorDerecho;
                    break;
                case '-':
                    resultado = valorIzquierdo - valorDerecho;
                    break;
                case '*':
                    resultado = valorIzquierdo * valorDerecho;
                    break;
                case '/':
                    resultado = valorIzquierdo / valorDerecho;
                    break;
            }
            pilaValores.Push(resultado);
        }
    }
}
