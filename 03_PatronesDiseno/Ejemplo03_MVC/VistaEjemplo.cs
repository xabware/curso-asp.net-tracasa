﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class VistaEjemplo: IVistaEjemplo
    {
        ControladorEjemplo controlador;

        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
        }

        public void menu()
        {
            int opcion;
            do
            {
                opcion = MostrarMenu<int>("Error, tu opción debe ser 1, 2, 3, 4 o 5.");
                GestionarOpcion(opcion);
            } while (true);
        }

        public void AltaEjemplo()
        {
            Console.WriteLine("numero:");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("string:");
            string str = Console.ReadLine();
            this.controlador.AltaEjemplo(entero, str);
        }
        public void MostrarEjemplos()
        {
            IEnumerable<Ejemplo> todos = controlador.LeerEjemplos();
            foreach (Ejemplo ejemplo in todos)
            {
                Console.WriteLine("Ejemplo: " + ejemplo.ToString());
            }
        }
        public void MostrarUno(string s)
        {
            Ejemplo ejemplo = controlador.LeerUno(s);
            if (ejemplo != null)
                Console.WriteLine("Ejemplo: " + ejemplo.ToString());
            else
                Console.WriteLine("No encontrado por " + s);
        }

        public void EliminarEjemplo(string s)
        {
            Ejemplo ejemplo = controlador.LeerUno(s);
            if (ejemplo != null)
            {
                if (controlador.EliminarUno(s))
                {
                    Console.WriteLine("Elemento eliminado con éxito");
                }
                else
                {
                    Console.WriteLine("Error al eliminar");
                }
            }
            else
            {
                Console.WriteLine("No encontrado por " + s);
            }
                
        }

        public void ModificarNombreEjemplo(string s)
        {
            Ejemplo ejemplo = controlador.LeerUno(s);
            if (ejemplo != null)
            {
                Console.Write("Introduce el nuevo nombre: ");
                string str = Console.ReadLine();
                if (controlador.ModificarNombreUno(ejemplo, str))
                {
                    Console.WriteLine("Elemento modificado con éxito");
                }
                else
                {
                    Console.WriteLine("Error al modificar");
                }
            }
            else
            {
                Console.WriteLine("No encontrado por " + s);
            }
        }

        public void ModificarEjemplo()
        {
            Console.WriteLine("Intro nombre a buscar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerUno(nombre) == null)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            }
            else
            {
                Console.WriteLine("Nuevo numero:");
                int entero = int.Parse(Console.ReadLine());
                Console.WriteLine("Nuevo string:");
                string str = Console.ReadLine();
                Ejemplo ej = this.controlador.Modificar(nombre, entero, str);

                if (ej != null)
                    Console.WriteLine("Modificado " + ej.ToString());
                else
                    Console.WriteLine("No encontrado por " + nombre);
            }
        }

        private Tipo MostrarMenu<Tipo>(string mensajeError)
        {
            bool errorFlag;
            string linea;
            Tipo varNum;
            do
            {

                Console.WriteLine("Opciones: ");
                Console.WriteLine();
                Console.WriteLine("    (1) Añadir nuevo Ejemplo.");
                Console.WriteLine();
                Console.WriteLine("    (2) Mostrar un Ejemplo.");
                Console.WriteLine();
                Console.WriteLine("    (3) Mostrar todos los Ejemplos.");
                Console.WriteLine();
                Console.WriteLine("    (4) Modificar el nombre de un ejemplo.");
                Console.WriteLine();
                Console.WriteLine("    (5) Modificar un ejemplo.");
                Console.WriteLine();
                Console.WriteLine("    (6) Eliminar un Ejemplo.");
                Console.WriteLine();
                Console.Write("Elige una opción: ");

                linea = Console.ReadLine();

                if (typeof(Tipo) == typeof(int))
                {
                    errorFlag = !int.TryParse(linea, out int tempInt);
                    if (tempInt < 1 && tempInt > 5)
                    {
                        errorFlag = true;
                    }
                    varNum = (Tipo)(object)tempInt;

                }
                else if (typeof(Tipo) == typeof(float))
                {
                    errorFlag = !float.TryParse(linea, out float tempFloat);
                    varNum = (Tipo)(object)tempFloat;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    errorFlag = !double.TryParse(linea, out double tempDouble);
                    varNum = (Tipo)(object)tempDouble;
                }
                else
                {
                    Console.Error.WriteLine("ERROR: Tipo de dato no valido");
                    throw new FormatException("ERROR: Tipo de dato no valido");
                }




                if (errorFlag)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(mensajeError);
                    Console.ForegroundColor = ConsoleColor.White;
                }

            } while (errorFlag);
            return varNum;
        }

        private string GestionarOpcion(int opcion)
        {
            string entrada=null;
            switch (opcion)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Añadir nuevo ejemplo.");
                    Console.ForegroundColor = ConsoleColor.White;
                    AltaEjemplo();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Mostrar un ejemplo.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el string de un ejemplo: ");
                    entrada = Console.ReadLine();
                    MostrarUno(entrada);
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Mostrar todos los ejemplos.");
                    Console.ForegroundColor = ConsoleColor.White;
                    MostrarEjemplos();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Modificar el nombre de un ejemplo.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el nombre del ejemplo a modificar: ");
                    entrada = Console.ReadLine();
                    ModificarNombreEjemplo(entrada);
                    break;
                case 5:
                    ModificarEjemplo();
                    break;
                case 6:
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Eliminar un ejemplo.");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.Write("Introduce el nombre del ejemplo: ");
                    entrada = Console.ReadLine();
                    EliminarEjemplo(entrada);
                    break;
                default:
                    Console.WriteLine("¿Cómo has llegado aquí?");
                    break;
            }
            return entrada;
        }
    }


}
