﻿using System;

namespace Ejemplo03_MVC
{
    class Program
    {
        IModeloEjemplo modelo;
        VistaEjemplo ve;
        ControladorEjemplo controlador;

        public Program()
        {
            this.modelo = new ModeloEjemDiccionario();
            controlador = new ControladorEjemplo(modelo);
            this.ve = new VistaEjemplo(controlador);
        }

        void ProbarDatos()
        {
            modelo.Crear(1, "Uno");
            modelo.Crear(2, "Dos");
            modelo.Crear(3, "Tres");

            
            ve.AltaEjemplo();
            ve.MostrarEjemplos();
        }

        static void Main(string[] args)
        {
            Program programa = new Program();
            programa.ve.menu();
        }
    }
}
