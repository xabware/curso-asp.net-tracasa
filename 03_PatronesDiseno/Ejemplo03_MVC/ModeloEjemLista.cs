﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemLista : IModeloEjemplo
    {
        private List<Ejemplo> ejemplos;

        public ModeloEjemLista()
        {
            ejemplos = new List<Ejemplo>();
        }

        public Ejemplo LeerUno(string str)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str == str)
                    return ejemplo;
            }
            return null;
        }

        public Ejemplo Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
            return ejemplo;
        }

        public Ejemplo Crear(int entero, string str)
        {
            return Crear(new Ejemplo(entero, str));
        }


        IEnumerable<Ejemplo> IModeloEjemplo.LeerTodos()
        {
            return ejemplos;
        }

        public bool EliminarUno(string nombre)
        {
            Ejemplo ejemplo = LeerUno(nombre);
            if (ejemplo != null)
            {
                return ejemplos.Remove(ejemplo);
            }
            return false;
        }

        public bool ModificarNombreUno(Ejemplo ejemplo, string str)
        {
            foreach (Ejemplo ej in ejemplos)
            {
                if (ej.Str == ejemplo.Str)
                {
                    ej.Str = str;
                    return true;
                }
                    
            }
            return true;
        }

        public Ejemplo ModificarUno(string nombreBusq, int entero, string str)
        {
            Ejemplo ejemploAmodif = LeerUno(nombreBusq);
            if (ejemploAmodif == null)
                return null;
            int posicion = ejemplos.IndexOf(ejemploAmodif);
            ejemplos[posicion] = new Ejemplo(entero, str);
            return ejemplos[posicion];
        }

        /* De esta manera, creamos al final de las lista, alteramos el orden */
        public Ejemplo Modificar2(string nombreBusq, int entero, string str)
        {
            EliminarUno(nombreBusq);
            return Crear(entero, str);
        }
    }
}
