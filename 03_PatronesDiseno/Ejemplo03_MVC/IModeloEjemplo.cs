﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo
    {
        Ejemplo Crear(int entero, string str);
        IEnumerable<Ejemplo> LeerTodos();
        Ejemplo LeerUno(string s);
        bool EliminarUno(string nombre);
        bool ModificarNombreUno(Ejemplo ejemplo, string str);
        Ejemplo ModificarUno(string nombreBusq, int entero, string str);
    }
}
