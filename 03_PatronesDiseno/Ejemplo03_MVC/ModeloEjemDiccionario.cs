﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    public class ModeloEjemDiccionario : IModeloEjemplo
    {
        Dictionary<string, Ejemplo> diccionario;

        public ModeloEjemDiccionario()
        {
            diccionario = new Dictionary<string, Ejemplo>();
        }
        public void Crear(string key, int entero, string str)
        {
            diccionario.Add(key, new Ejemplo(entero, str));
        }



        public Ejemplo Crear(int entero, string str)
        {
            Ejemplo ejemplo = new Ejemplo(entero, str);
            diccionario.Add(str+"-"+entero, ejemplo);
            return ejemplo;
        }
        public Ejemplo Crear(Ejemplo ejemplo)
        {
            throw new NotImplementedException("No se puede por la clave");
            //TODO: Leer propiedad privada mediante REFLECTION
        }

        public bool EliminarUno(string nombre)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombre))
                {
                    return diccionario.Remove(item.Key);
                }
            }
            return false;
        }

        public IEnumerable<Ejemplo> LeerTodos()
        {
            List<Ejemplo> lista = new List<Ejemplo>();
            foreach(var item in diccionario)
            {
                lista.Add(item.Value);
            }
            return lista;
        }

        public Ejemplo LeerUno(string s)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(s))
                {
                    return item.Value;
                }
            }
            return null;
        }

        public bool ModificarNombreUno(Ejemplo ejemplo, string str)
        {
            ejemplo.Str = str;
            return true;
        }

        public Ejemplo ModificarUno(string nombreBusq, int entero, string str)
        {
            foreach (var item in diccionario)
            {
                if (item.Value.Str.Equals(nombreBusq))
                {
                    Ejemplo nuevoEjemploModificado = new Ejemplo(entero, str);
                    string clave = item.Key;
                    diccionario[clave] = nuevoEjemploModificado;
                    return nuevoEjemploModificado; // item.Value;
                }
            }
            return null;
        }
    }
}
