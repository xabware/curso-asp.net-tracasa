﻿using System;
using System.Collections.Generic;

namespace Ejemplo01_Singleton
{
    class GestorTextos
    {
        private List<string> textos;

        private static GestorTextos instancia;

        private GestorTextos()
        {
            textos = new List<string>();
        }

        /*
         * Vale tambien en java
        public static GestorTextos GetInstancia()
        {
            if (instancia == null)
            {
                instancia = new GestorTextos();
            }
            return instancia;
        }*/

        public static GestorTextos Instancia
        {
            get
            {
                if (instancia == null)
                {
                    instancia = new GestorTextos();
                }
                return instancia;
            }
        }

        public void Nuevo(string t)
        {
            textos.Add(t);
        }
        public void Mostrar()
        {
            foreach (string s in textos)
            {
                Console.WriteLine(s);
            }
        }
    }
}
