﻿using System;

namespace Ejemplo01_Singleton
{
    class Modulo2
    {
        public static void Main(string[] args)
        {
            GestorTextos gt = GestorTextos.Instancia;
            gt.Nuevo("DDDD");
            gt.Nuevo("EEEE");
            gt.Nuevo("FFFF");
            gt.Mostrar();
            Modulo1.Main2(null);
        }
    }
}
