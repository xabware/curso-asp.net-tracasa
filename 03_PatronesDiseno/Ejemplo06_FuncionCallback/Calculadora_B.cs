﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_Funcion_Callback
{
    class Calculadora_B
    {
        static public float SumarB(float a1, float a2)
        {
            return a1 + a2;
        }
        static public float MultiplicarB(float a1, float a2)
        {
            return a1 * a2;
        }
    }
}
