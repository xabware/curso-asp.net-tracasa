﻿using System;

namespace Ejemplo06_Funcion_Callback
{
    // Creamos un nuevo tipo de dato, que indica que es una función
    // (ni clase, ni interfaz...). Donde lo que importa son los 
    // tipos de datos que recibe y que devuelve. 
    // Es decir, declaramos un delegado.
    delegate float FuncionOperador(float op1, float op2);
    //Ahora podemos crear variables de tipo funcion
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine("Invocamos con suma");
            // Al pasar como parámetro la función SumarA, no lleva 
            // paréntesis, porque NO se llama ahora, sino después
            VistaCalculadora(Calculadora_B.SumarB);
            Console.WriteLine("Invocamos con multiplicador");
            VistaCalculadora(Calculadora_B.MultiplicarB);*/
            VistaOperadora(Calculadora_A.SumarA,Calculadora_B.MultiplicarB);
        }
        // Esta funcion necesita de una función callback para
        // saber como tiene que calcular. 
        static void VistaCalculadora(FuncionOperador operador)
        {
            Console.WriteLine("Operando 1:");
            float x = float.Parse(Console.ReadLine());
            Console.WriteLine("Operando 2:");
            float y = float.Parse(Console.ReadLine());
            float resultado = operador(x, y);
            Console.WriteLine("Resultado: " + resultado);
        }
        static void VistaOperadora(FuncionOperador suma, FuncionOperador multiplica)
        {
            float x=0, y=0;
            Console.Write("Introduce dos numeros separados por un operador con espacios entre si: ");
            string linea = Console.ReadLine();
            string[] cosas = linea.Split(" ");
            bool bien;
            bien = float.TryParse(cosas[0],out x);
            if (bien)
            {
                bien = float.TryParse(cosas[2], out y);
            }
            string operador = cosas[1];
            float resultado=0;
            if (bien)
            {
                switch (operador)
                {
                    case "+":
                        resultado = suma(x, y);
                        break;
                    case "*":
                        resultado = multiplica(x, y);
                        break;
                    default:
                        bien = false;
                        break;
                }
            }
            if (bien)
            {
                Console.WriteLine("Resultado: " + resultado);
            }
            else
            {
                Console.WriteLine("No te entiendo, humano");
            }
        }
        //Ejercicio: Usar vista calculadora con una calculadora creada por nosotros, pero exacta
    }
}
