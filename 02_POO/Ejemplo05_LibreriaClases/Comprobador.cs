﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public class Comprobador
    {

        public static string IntroducirTexto(string mensaje)
        {
            string varDato;
            Console.WriteLine(mensaje);
            do
            {
                varDato = Console.ReadLine();
                // Validación del dato
            } while (string.IsNullOrEmpty(varDato));
            return varDato;
        }


        //Para pasar un tipo de dato definido por el usuario del metodo están los templates o métodos genéricos
        public static Tipo IntroducirNumero<Tipo>(string mensaje, string mensajeError)
        {
            bool errorFlag;
            string linea;
            Tipo varNum;
            do
            {

                Console.Write(mensaje);
                linea = Console.ReadLine();

                if (typeof(Tipo) == typeof(int))
                {
                    errorFlag = !int.TryParse(linea, out int tempInt);
                    varNum = (Tipo)(object)tempInt;
                }else if(typeof(Tipo) == typeof(float))
                {
                    errorFlag = !float.TryParse(linea, out float tempFloat);
                    varNum = (Tipo)(object)tempFloat;
                }else if(typeof(Tipo) == typeof(double))
                {
                    errorFlag = !double.TryParse(linea, out double tempDouble);
                    varNum = (Tipo)(object)tempDouble;
                }else
                {
                    Console.Error.WriteLine("ERROR: Tipo de dato no valido");
                    throw new FormatException("ERROR: Tipo de dato no valido");
                }
                

                if (errorFlag)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(mensajeError);
                    Console.ForegroundColor = ConsoleColor.White;
                }

            } while (errorFlag);
            return varNum;
        }


    }
}
