﻿
namespace Ejemplo01_Encapsulacion
{
     public interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }
    }
}
