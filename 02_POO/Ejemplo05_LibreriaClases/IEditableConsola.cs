﻿
namespace Ejemplo01_Encapsulacion
{
    public interface IEditableConsola
    {
        void PedirDatos();
        void MostrarDatos();
    }
}
