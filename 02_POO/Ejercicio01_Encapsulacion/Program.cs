﻿using System;

namespace Ejercicio01_Encapsulacion
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario miUsuario = new Usuario("Legal", 15, 1.68f);

            Usuario unHacker = new Usuario("\"\"", -20, 0f);
            Console.WriteLine("Altura legal: " + miUsuario.Altura);
            Console.WriteLine("Edad legal: " + miUsuario.Edad);
            Console.WriteLine("Nombre legal: " + miUsuario.Nombre);
            Console.WriteLine("Altura ilegal: " + unHacker.Altura);
            Console.WriteLine("Edad ilegal: " + unHacker.Edad);
            Console.WriteLine("Nombre ilegal: " + unHacker.Nombre);
        }
    }
}
