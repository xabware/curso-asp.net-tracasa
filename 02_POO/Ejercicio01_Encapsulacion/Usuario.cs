﻿using Ejemplo01_Encapsulacion;
using System;

namespace Ejercicio01_Encapsulacion
{
    public class Usuario : INombrable, IEditableConsola
    {
        string nombre;
        int edad;
        float altura;

        public Usuario(string nombre, int edad, float altura)
        {
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario()
        {
        }

        public string Nombre
        {
            get
            {
                return GetNombre();
            }
            set
            {
                SetNombre(value);
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if (value > 0)
                {
                    edad = value;
                }
                else
                {
                    edad = 0;
                }
            }
        }

        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if (value > 0.1f)
                {
                    altura = value;
                }
                else
                {
                    altura = 0.1f;
                }
            }
        }

        public override string ToString()
        {
            return "El usuario " + Nombre + ", tiene una edad de " + Edad + " años, y una altura de " + Altura + " metros.";
        }

        public string GetNombre()
        {
            return nombre;
        }

        public void SetNombre(string unNombre)
        {
            if (unNombre != null && unNombre != "\"\"")
            {
                nombre = unNombre;
            }
            else
            {
                nombre = "tempname";
            }
        }

        public virtual void PedirDatos()
        {
            Console.Write("Introduce el nombre de usuario: ");
            Nombre = Console.ReadLine();
            Edad = Comprobador.IntroducirNumero<int>("Introduce la edad del usuario: ","Error, introduce un número entero positivo.");
            Altura = Comprobador.IntroducirNumero<float>("Introduce la altura del usuario: ", "Error, introduce un número decimal positivo.");
        }

        public virtual void MostrarDatos()
        {
            Console.WriteLine("El usuario " + Nombre + ", tiene una edad de " + Edad + " años, y una altura de " + Altura + " metros");
        }
    }
}
