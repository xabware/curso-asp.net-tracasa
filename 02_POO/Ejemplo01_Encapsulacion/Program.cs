﻿using System;

namespace Ejemplo01_Encapsulacion
{
    class Program
    {

        static void Main(string[] args)  
        {
            Coche miCoche = new Coche();
            miCoche.Acelerar();
            miCoche.Modelo = "Kia";
            miCoche.Precio = -20;

            Console.WriteLine("Velocidad: " + miCoche.Velocidad);
            Console.WriteLine("Modelo: " + miCoche.Modelo);



        }
    }
}
