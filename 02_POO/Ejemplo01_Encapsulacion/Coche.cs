﻿using System;

namespace Ejemplo01_Encapsulacion
{
    public class Coche : Object, INombrable, IEditableConsola
    {
        float velocidad;
        string modelo;
        protected int precio;

        public Coche(string modelo, int precio)
        {
            Modelo = modelo;
            Precio = precio;
        }

        public Coche(string marca, string modelo, int precio)
        {
            Modelo = modelo;
            Precio = precio;
            Marca = marca;
        }

        public Coche()
        {
            Modelo = "";
            Precio = 0;
        }


        public float Velocidad
        {
            get
            {
                return velocidad;
            }
        }

        public string Modelo 
        {
            get
            {
                return modelo;
            }

            set
            {
                this.modelo = value;
            }       
        }

        public string Nombre
        {
            get
            {
                return GetNombre();
            }
            set
            {
                SetNombre(value);
            }
        }

        public string GetNombre()
        {
            return Marca + " - " + Modelo;
        }

        public void SetNombre(string unNombre)
        {
            if (!string.IsNullOrEmpty(unNombre))
            {
                string[] separados = unNombre.Split("-");
                Marca = separados[0].Trim();
                if (separados.Length > 1)
                {
                    Modelo = separados[1].Trim();
                }
            }
        }
        /// <summary>
        /// Establece tanto marca como modelo
        /// </summary>
        /// <param name="unNombre">Recibe "Marca - Modelo"</param>
        public int Precio
        {
            set
            {
                if (value >= 0)
                    precio = value;
                else
                    precio = 0;
            }
        }

        // Hay una manera de hacerlo resumido:
        public string Marca
        {
            get;    // Ya crea una variable interna __marca o algo así, y la usa
            set;    // de la manera común
        }

        public virtual void Acelerar()
        {
            velocidad++;
        }

        public override bool Equals(object obj)
        {
            if(base.Equals(obj))
            {
                return true;
            }
            else
            {
                Coche comp = (Coche)obj;
                return (this.modelo==comp.modelo && this.precio==comp.precio && this.Marca.Equals(comp.Marca));
            }
        }

        public override string ToString()
        {
            return ("Coche: "+Marca+"-"+Modelo+". precio: "+precio+".");
        }

        public virtual void PedirDatos()
        {
            Console.Write("Introduce la marca del vehículo: ");
            Marca = Console.ReadLine();
            Console.Write("Introduce el modelo del vehículo: ");
            Modelo = Console.ReadLine();
            Precio = Comprobador.IntroducirNumero<int>("Introduce el precio del vehículo: ", "Error, introduce un número entero positivo");
        }

        public virtual void MostrarDatos()
        {
            Console.WriteLine(ToString());
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
