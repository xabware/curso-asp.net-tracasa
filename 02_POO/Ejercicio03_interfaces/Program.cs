﻿using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;
using Ejercicio2_Herencia;
using herencias;
using System;
using System.Collections.Generic;

namespace Ejercicio03_interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
             * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
             *      Hacer que el usuario que ya tenemos implemente la interfaz 
             *      INombrable y usar los 2 métodos y la propiedad
             * 3 -  Crear un array de INombrable con un empleado y su coche electrico
             * 4 -  Implementar la interfaz IClonable en CocheElectrico:
             *      El método tiene que instanciar un nuevo obj y asignar 
             *      las propiedades del nuevo coche con sus propias propiedades
             * 5 - Crear una nueva interfaz que obligue a implementar 2 métodos, 
             * uno para mostrar directamente los datos por consola y otro para pedir 
             * sus datos por consola
             * 6 - Implementar dicha interfaz en Usuario y en Coche
             * sobreescribir los métodos en Empleado ( si quereis en CElectrico)
             * 7- Usar los métodos en un nuevo usuario y en el empleado del ejercicio 3 y en un Coche (y si quereis en cocheElectrico)
              */
            /*INombrable[] vector = new INombrable[2];
            vector[0] = new CocheElectrico("roadster", 60000, "Tesla", 100);
            vector[1] = new Empleado("pepe", 30, 2.6f, 30000);
            
            Coche miCoche = new Coche();
            miCoche.PedirDatos();
            miCoche.MostrarDatos();
            CocheElectrico miTesla = new CocheElectrico();
            miTesla.PedirDatos();
            miTesla.MostrarDatos();
            ((Empleado)vector[1]).SuCoche = (CocheElectrico)vector[0];
            ((IEditableConsola)vector[1]).MostrarDatos();

            CocheElectrico mismoElectrico = (CocheElectrico)vector[0];
            CocheElectrico copiaElectrico = (CocheElectrico)((CocheElectrico)vector[0]).Clone();
            mismoElectrico.Modelo = "nuevoModelo";
            copiaElectrico.Modelo = "modeloDelClon";
            ((IEditableConsola)vector[1]).MostrarDatos();
            copiaElectrico.MostrarDatos();*/
            EjemploListaCoches();
        }

        static void EjemploLista()
        {
            List<string> textos = new List<string>();
            textos.Add("primer texto");
            textos.Add("segundo texto");
            textos.Add("tercer texto");
            textos.Add("otro texto");
            for(int i = 0; i < 30; i++)
            {
                textos.Add("Texto: " + i);
            }
            textos.RemoveAt(2);
            textos.Remove("otro texto");
            textos.ForEach(Console.WriteLine);
        }
        static void EjemploListaCoches()
        {
            List<Coche> coches = new List<Coche>();
            coches.Add(new Coche("Toyota", "Cupra", 50000));
            coches.Add(new CocheElectrico("Roadster",60000,"Tesla",100));
            coches[0].MostrarDatos();

            IList<Coche> icoches = coches;
            icoches.Add(new Coche());
            IList<Usuario> listaUsu = new List<Usuario>();
            listaUsu.Add(new Usuario("Usuario de Lista 1", 0, 0));
            listaUsu.Add(new Usuario("Usuario de Lista 2", 0, 0));

            IList<Usuario> arrayUsu = new Usuario[10];
            arrayUsu[0] = new Usuario("Usuario de Lista 2", 0, 0);
            arrayUsu[1] = new Usuario("Usuario de Lista 2", 0, 0);

            MostrarColeccion(listaUsu);
            MostrarColeccion(arrayUsu);
        }

        static void MostrarColeccion(ICollection<Usuario> icoleccion)
        {
            Console.WriteLine("Coleccion usuarios " + icoleccion.GetType());
            foreach (Usuario usu in icoleccion)
            {
                if(usu != null)
                {
                    usu.MostrarDatos();
                }
                
            }
        }
    }
}
