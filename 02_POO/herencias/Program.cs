﻿using System;
using Ejemplo01_Encapsulacion;

namespace herencias
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            Coche miCoche = new Coche("SeatPandaPorLaRajaDeTuFalda",600);
            Coche miCocheB = new Coche("SeatPandaPorLaRajaDeTuFalda",600);
            Coche miCoche2 = new Coche("SeatLeonConLlantasDelFeuVert", 700);
            miCoche.Acelerar();

            Console.WriteLine("To string 1: " + miCoche.ToString());
            Console.WriteLine("HashCode1: " + miCoche.GetHashCode());
            Console.WriteLine("HashCode2: " + miCoche2.GetHashCode());
            Console.WriteLine("Tipo: " + miCoche2.GetType());
            if (miCoche.GetType().Equals(miCoche2.GetType()))
            {
                Console.WriteLine("Los dos son coches");
            }
            if (miCoche.Equals(miCoche2))
            {
                Console.WriteLine("1 y 2 son iguales");
            }
            if (miCoche.Equals(miCocheB))
            {
                Console.WriteLine("1 y B son iguales");
            }
            */
            CocheElectrico miCocheElectrico = new CocheElectrico("Roadster",1,"Tesla",100);
            miCocheElectrico.Acelerar();
            //Console.WriteLine(miCocheElectrico.ToString());

            Coche miTeslaComoCoche = miCocheElectrico;
            Object miTeslaComoObjeto = miCocheElectrico;
            Console.WriteLine(miTeslaComoCoche);
            miTeslaComoCoche.Acelerar();
            Console.WriteLine(miTeslaComoCoche);
        }
    }
}
