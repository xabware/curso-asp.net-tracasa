﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace herencias
{
    public class CocheElectrico : Coche, ICloneable

    {
        double nivelBateria=100;

        public double NivelBateria
        {
            get
            {
                return nivelBateria;
            }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    nivelBateria = value;
                }
            }
        }

        public override void Acelerar()
        {
            if (NivelBateria > 0)
            {
                base.Acelerar();
                NivelBateria--;
            }
            
        }

        public CocheElectrico(string modelo, int precio, string marca ,double nivelBateria)
        {
            NivelBateria = nivelBateria;
            this.Modelo = modelo;
            this.Precio = precio;
            this.Marca = marca;
        }

        public CocheElectrico()
        {
        }

        public override string ToString()
        {
            return base.ToString()+" Batería: " + NivelBateria;
        }

        public object Clone()
        {
            CocheElectrico nuevo = new CocheElectrico(Modelo, precio, Marca, nivelBateria);
            return nuevo;
        }

        public override void PedirDatos()
        {
            base.PedirDatos();
            NivelBateria = Comprobador.IntroducirNumero<int>("Introduce el nivel de bateria del vehículo: ", "Error, introduce un número entero entre 0 y 100.");
        }

        public override void MostrarDatos()
        {
            Console.WriteLine(ToString());
        }
    }
}
