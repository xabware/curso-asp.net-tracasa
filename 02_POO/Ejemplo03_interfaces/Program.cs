﻿using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;
using herencias;
using System;

namespace Ejemplo03_interfaces
{
    class Program
    {
        static void Main(string[] args) 
        {
            //No suele ser aconsejable crear arrays de objetos. Esto es solo educativo.
            object[] popurri = new object[3];
            popurri[0] = new Coche("seat","leon", 6000);
            popurri[1] = new CocheElectrico("brumbrum", 400, "fiat", 100);
            popurri[2] = new Usuario("Juan", 25, 1.68f);

            foreach (object x in popurri)
            {
                Console.WriteLine(x.ToString());
            }
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche)popurri[0];
            Console.WriteLine(fiatPunto.GetNombre());
            // El polimorfismo se puede usar con interfaces
            INombrable fiatNombrable = /*(INombrable)*/ fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre);

            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.Nombre);

            /* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
             * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
             *      Hacer que el usuario que ya tenemos implemente la interfaz 
             *      INombrable y usar los 2 métodos y la propiedad
             * 3 -  Crear un array de INombrable con un empleado y su coche electrico
             * 4 -  Implementar la interfaz IClonable en CocheElectrico:
             *      El método tiene que instanciar un nuevo obj y asignar 
             *      las propiedades del nuevo coche con sus propias propiedades
             * 5 - Crear una nueva interfaz que obligue a implementar 2 métodos, 
             * uno para mostrar directamente los datos por consola y otro para pedir 
             * sus datos por consola
             * 6 - Implementar dicha interfaz en Usuario y en Coche
             * sobreescribir los métodos en Empleado ( si quereis en CElectrico)
             * 7- Usar los métodos en un nuevo usuario y en el empleado del ejercicio 3 y en un Coche (y si quereis en cocheElectrico)
              */

        }
    }
}
