﻿using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta pera = new Fruta("pera limonera",250);
            Console.WriteLine(pera.ToString());
            Console.WriteLine(pera.FormatearNombre());
            Console.WriteLine(pera.FormatearNombreN("muchwow: "));
            int[] unArray = new int[] { 1, 2, 3 };
            Console.WriteLine(unArray.ToUpperString());
            string[] otroArray = new string[] { "uno", "dos", "tres" };
            Console.WriteLine(unArray.ToUpperString());

        }

        //En c# se puede añadir métodos a unas clases desde otras clases
        
    }

    public static class OtroModulo
    {
        //Mediante una función estática, con esta estructura:
        public static string FormatearNombre(this Fruta laFruta)
        {
            string nombreFormateado = "FRUTA: " + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }
        public static string FormatearNombreN(this Fruta laFruta,string mensaje)
        {
            string nombreFormateado = mensaje + laFruta.Nombre.ToUpper();
            return nombreFormateado;
        }

        public static string ToString(this string[] array)
        {
            string res = "";
            foreach (string item in array)
            {
                res = String.Concat(res, item);
            }
            return res;
        }

        public static string ToUpperString(this object CualquierObjeto)
        {
            return CualquierObjeto.ToString().ToUpper();
        }
        
    }
}
