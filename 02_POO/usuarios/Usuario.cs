﻿using Ejemplo01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;

namespace usuarios
{
    public class Usuario : INombrable
    {
        string nombre;
        int edad;
        float altura;

        public Usuario(string nombre, int edad, float altura)
        {
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }

        public Usuario()
        {
        }

        public string Nombre
        {
            get
            {
                return GetNombre();
            }
            set
            {
                SetNombre(value);
            }
        }

        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if (value > 0)
                {
                    edad = value;
                }
                else
                {
                    edad = 0;
                }
            }
        }

        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if (value > 0.1f)
                {
                    altura = value;
                }
                else
                {
                    altura = 0.1f;
                }
            }
        }

        public void mostrarDatos()
        {
            Console.WriteLine("El usuario " + Nombre + ", tiene una edad de " + Edad + " años, y una altura de " + Altura + " metros");
        }

        public override string ToString()
        {
            return "El usuario " + Nombre + ", tiene una edad de " + Edad + " años, y una altura de " + Altura + " metros.";
        }

        public string GetNombre()
        {
            return nombre;
        }

        public void SetNombre(string unNombre)
        {
            if (unNombre != null && unNombre != "\"\"")
            {
                nombre = unNombre;
            }
            else
            {
                nombre = "tempname";
            }
        }
    }
}
