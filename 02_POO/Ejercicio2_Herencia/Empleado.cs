﻿using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;
using System;

namespace Ejercicio2_Herencia
{
    public class Empleado : Usuario
    {
        /*añadir campo salario
         Crear método sobreescribiendo ToString
        tanto en Usuario como en Empleado
        Con todos los constructores necesarios en ambas clases
        */
        int salario;
        Coche suCoche;

        public Coche SuCoche
        {
            get { return suCoche; }
            set { suCoche = value; }
        }

        public int Salario
        {
            get
            {
                return salario;
            }
            set
            {
                salario = value;
            }
        }

        public Empleado(string nombre, int edad, float altura, int salario) : base(nombre, edad, altura)
        {
            Salario = salario;
        }

        public Empleado() : base("", 1, 0)
        {
        }

        public override string ToString()
        {
            return base.ToString()+" Además su salario es de "+Salario + (SuCoche==null ? ".":", y su coche es "+SuCoche);

        }

        public override void PedirDatos()
        {
            base.PedirDatos();
            Salario = Comprobador.IntroducirNumero<int>("Introduce el salario del usuario: ", "Error, introduce un número entero positivo.");
        }

        public override void MostrarDatos()
        {
            Console.WriteLine(ToString());
        }
    }
}
