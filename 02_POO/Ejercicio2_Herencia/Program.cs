﻿using System;
using Ejemplo01_Encapsulacion;
using Ejercicio01_Encapsulacion;

namespace Ejercicio2_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Crear un array de usuarios con 2 usuarios y 2 empleados y recorrer el array empleando toString*/
            Usuario[] gente = new Usuario[4];
            gente[0] = new Usuario("SeñorNormal", 25, 1.75f);
            gente[1] = new Usuario("JUAN", 999, 999f);
            gente[2] = new Empleado("El currante de turno", 40, 1.90f,10);
            gente[3] = new Empleado("El jefe, no hace nada pero nos cae bien así que le pagamos", 60, 1.80f,99999);
            Console.WriteLine(gente[0].GetType());
            Console.WriteLine(gente[2].GetType());
            for(int i = 0; i < 4; i++)
            {
                if (gente[i].GetType().ToString() == "Ejercicio2_Herencia.Empleado")
                {
                    Empleado empl = (Empleado)gente[i];
                    empl.Salario += 10;
                }
                Console.WriteLine(gente[i].ToString());
            }
            /*En este ejemplo de polimorfismo, llamamos al mismo método sobre objetos de distinta clase en el bucle. 
              El mismo método tiene distintos resultados.*/
        }
    }
}
